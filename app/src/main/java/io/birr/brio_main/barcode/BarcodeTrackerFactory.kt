package io.birr.brio_main.barcode

import android.content.Context

import com.google.android.gms.vision.MultiProcessor
import com.google.android.gms.vision.Tracker
import com.google.android.gms.vision.barcode.Barcode

/**
 * Factory for creating a tracker and associated graphic to be associated with a new barcode.  The
 * multi-processor uses this factory to create barcode trackers as needed -- one for each barcode.
 */
internal class BarcodeTrackerFactory(private val mContext: Context) : MultiProcessor.Factory<Barcode> {

	override fun create(barcode: Barcode): Tracker<Barcode> {
		return BarcodeTracker(mContext)
	}
}
