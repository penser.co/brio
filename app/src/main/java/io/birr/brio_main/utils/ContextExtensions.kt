package io.birr.brio_main.utils

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.View

val View.ctx: Context get() = context

fun Context.color(colorId: Int): Int {
	return ContextCompat.getColor(this, colorId)
}
