package io.birr.brio_main.utils

import android.Manifest
import android.app.Activity
import android.os.Build
import android.telephony.SmsManager
import io.birr.brio_main.model.getPrefInt

fun Activity.sendSMS(to: String, msg: String) {
	if (Build.VERSION.SDK_INT >= 22) {
		withPermission(Manifest.permission.READ_PHONE_STATE) {
			val sim = getSimList()?.get(getPrefInt("selected_sim")) ?: 0

			withPermission(Manifest.permission.SEND_SMS) {
				SmsManager.getSmsManagerForSubscriptionId(sim)
					.sendTextMessage(to, null, msg, null, null)
			}
		}
	} else {
		SmsManager
			.getDefault()
			.sendTextMessage(to, null, msg, null, null)
	}
}