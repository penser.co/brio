package io.birr.brio_main.utils.UI

import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.view.View

fun Context.color(color: Int): Int {
	return ContextCompat.getColor(this, color)
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun View.tint(color: Int) {
	backgroundTintList = ColorStateList.valueOf(
		context.color(color)
	)
}