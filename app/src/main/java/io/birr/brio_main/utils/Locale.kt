package io.birr.brio_main.utils

import android.app.Activity
import io.birr.brio_main.model.getPrefString
import io.birr.brio_main.model.setPref
import io.birr.brio_main.utils.AppLocale.Companion.LOCALE_EN
import io.birr.brio_main.utils.AppLocale.Companion.REGION_MASCULINE
import io.birr.brio_main.utils.AppLocale.Companion.SUPPORTED_LOCALES
import java.util.*

class AppLocale {
	companion object {

		const val LOCALE_EN = "en"
		const val LOCALE_AM = "am"

		val SUPPORTED_LOCALES = listOf(
			LOCALE_EN, LOCALE_AM
		)

		/**
		 * XF and XM are not actually regions (duh!), it's just a hack
		 * for easily creating amharic variants for formal, feminine
		 * and masculine translations
		 */
		const val REGION_FEMININE = "XF"
		const val REGION_MASCULINE = "XM"
	}
}

fun Activity.refreshLocale() {
	val locale = getPrefString("locale")
	val language = when (locale) {
		in SUPPORTED_LOCALES -> locale
		else -> {
			setPref("locale", LOCALE_EN)
			LOCALE_EN
		}
	}

	val res = this.resources
	val dm = res.displayMetrics
	val conf = res.configuration

	conf.locale = Locale(language, REGION_MASCULINE)
	res.updateConfiguration(conf, dm)
}

