package io.birr.brio_main.utils.services

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import android.telephony.TelephonyManager
import io.birr.brio_main.ui.activities.MainActivity
import org.jetbrains.anko.startActivity

class CallService : Service() {
	override fun onBind(intent: Intent?): IBinder? {
		return null
	}

	override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
		super.onStartCommand(intent, flags, startId)

		watchCbeCall()
		return START_STICKY
	}

	private fun watchCbeCall() {
		val receiver = object : BroadcastReceiver() {
			override fun onReceive(context: Context, intent: Intent) {

				val state = intent.getStringExtra(TelephonyManager.EXTRA_STATE)

				if (state == null) {
					val number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER)

					if (
						intent.action == "android.intevnt.action.NEW_OUTGOING_CALL" &&
						number.matches("""\*847.*#""".toRegex())
					) {
						context.startActivity<MainActivity>()
					}
				}
			}
		}

		val filter = IntentFilter(Intent.ACTION_SCREEN_OFF)
		registerReceiver(receiver, filter)
	}
}
