package io.birr.brio_main.utils

import android.app.Activity
import android.content.Context
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import io.birr.brio_main.R

fun Context.withPermission(permission: String, onGranted: () -> Unit) {
	withPermission(permission, onGranted) {}
}

fun Context.withPermission(permission: String, onGranted: () -> Any, onDenied: () -> Any) {
	Dexter.withActivity(this as Activity)
		.withPermission(permission)
		.withListener(object : PermissionListener {
			override fun onPermissionGranted(response: PermissionGrantedResponse) {
				onGranted()
			}

			override fun onPermissionDenied(response: PermissionDeniedResponse) {
				toast(getString(R.string.need_permission), Toast.LENGTH_LONG)
				onDenied()
			}

			override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
				token.continuePermissionRequest()
			}
		}).check()
}

fun Context.checkPermissions(vararg permissions: String, callback: () -> Any = fun() {}) {
	Dexter.withActivity(this as Activity)
		.withPermissions(permissions.toList())
		.withListener(object : MultiplePermissionsListener {

			override fun onPermissionsChecked(report: MultiplePermissionsReport) {
				callback()
			}

			override fun onPermissionRationaleShouldBeShown(permission: List<PermissionRequest>, token: PermissionToken) {
				token.continuePermissionRequest()
			}
		}).check()
}