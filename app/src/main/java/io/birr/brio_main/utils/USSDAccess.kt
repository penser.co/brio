package io.birr.brio_main.utils

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.AccessibilityServiceInfo
import android.view.accessibility.AccessibilityEvent

class USSDAccess : AccessibilityService() {

	override fun onAccessibilityEvent(event: AccessibilityEvent) {

		val text: String = event.text.toString()

//		broadcast("TOAST", Pair("text", text))

		if (event.className == "android.app.AlertDialog") {
			when {
				text.matches("""(?i)^welcome\s*to\s*cbe\s*birr.*\n.*""".toRegex()) -> {
					broadcast("CBE_REGISTERED")
					performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK)
				}
				text.matches("""(?i)^welcome\s*to\s*cbe\s*birr\.%""".toRegex()) -> {
					broadcast("CBE_NOT_REGISTERED")

					performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK)
				}
				else -> {

				}
			}
		}
	}

	override fun onInterrupt() {}

	override fun onServiceConnected() {
		super.onServiceConnected()

		val info = AccessibilityServiceInfo()

		info.flags = AccessibilityServiceInfo.DEFAULT
		info.packageNames = arrayOf("com.android.number")
		info.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED
		info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC

		serviceInfo = info
	}
}