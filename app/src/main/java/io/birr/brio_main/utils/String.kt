package io.birr.brio_main.utils

fun String.formatPhoneNumber(): String {
	val spacingRegex = Regex("^(09[0-9])([0-9]{3})([0-9]{4})")

	return replace(spacingRegex, {
		"${it.groupValues[1]} ${it.groupValues[2]} ${it.groupValues[3]}"
	})
}