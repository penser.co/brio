package io.birr.brio_main.utils

import android.content.Context
import io.birr.brio_main.R
import io.birr.brio_main.model.getPrefString
import io.birr.brio_main.utils.AppLocale.Companion.LOCALE_AM
import io.birr.brio_main.utils.AppLocale.Companion.REGION_FEMININE
import io.birr.brio_main.utils.AppLocale.Companion.REGION_MASCULINE
import java.text.SimpleDateFormat
import java.util.*

fun convertFromJulian(day: Int, month: Int, year: Int): Map<String, Int> {
	val OFFSET = 79372
	val DAYS = 86400000 // A day converted from a UTC (1000 * 60 * 60 * 24)

	val days = getEthiopicDate(OFFSET, DAYS, year, month, day)
	var eYear: Int// This is because ethiopian calender year is less by 8 from the Gregorian

	//approximation with errors on eYear %4 = 0
	val yearsApplied = Math.floor(days / 365.25).toInt()
	eYear = (1745 + yearsApplied)
	var daysRemaining: Int = (days - Math.floor(yearsApplied * 365.25)).toInt()

	var eMonth = 0
	var eDate = 0

	//fix the approximation error
	if (eYear % 4 == 0) {
		daysRemaining--
	}

	if (daysRemaining == 0) {
		eYear--
		eMonth = 13
		eDate = 5

		if (eYear % 4 == 3) eDate += 1
	} else {
		eMonth = Math.ceil(daysRemaining.toDouble() / 30.0).toInt()
		eDate = if (daysRemaining % 30 == 0) 30 else daysRemaining % 30
	}

	return mapOf(
		"day" to eDate,
		"month" to eMonth,
		"year" to eYear
	)
}

fun getEthiopicDate(offset: Int, day: Int, fullYear: Int, fullMonth: Int, fullDay: Int): Long {
	val UTCVARX = GregorianCalendar(fullYear, fullMonth - 1, fullDay, 3, 0, 0);
	return (offset + (UTCVARX.timeInMillis / day)) //number of days since epoch (1/1/1970) plus offset
}

fun Date.toAmharicDate(): Map<String, Int> {
	val day = SimpleDateFormat("d", Locale.ENGLISH).format(this)
	val year = SimpleDateFormat("yyyy", Locale.ENGLISH).format(this)
	val month = SimpleDateFormat("MM", Locale.ENGLISH).format(this).toInt()

	return convertFromJulian(day.toInt(), month, year.toInt());
}

fun Int.toAmharicMonthName(): String {
	if (this !in 1..13) return ""

	val amharicMonths = arrayOf(
		"መስከረም", "ጥቅምት", "ኅዳር", "ታኅሣሥ",
		"ጥር", "የካቲት", "መጋቢት", "ሚያዝያ",
		"ግንቦት", "ሰኔ", "ሐምሌ", "ነሐሴ", "ጳግሜ"
	)

	return amharicMonths[this - 1]
}

fun Date.formatDate(context: Context): String {
	//let store the current date in amharic just in case
	val amDate = toAmharicDate()

	when (context.getPrefString("locale")) {
		in listOf(LOCALE_AM) -> {
			val hour = getLocalizedTime(context)
			val weekDay = getWeekDayString(context)
			val month = amDate["month"]!!.toInt().toAmharicMonthName()
			val day = amDate["day"]
			val year = amDate["year"]

			return "$weekDay — $month $day, $year — $hour"
		}

		else -> {
			val weekDay = getWeekDayString(context)
			val month = getMonthString(context)
			val hour = getLocalizedTime(context)
			val day = SimpleDateFormat("d", Locale.ENGLISH).format(this)
			val year = SimpleDateFormat("yyyy", Locale.ENGLISH).format(this)
			return "$weekDay — $month $day, $year — $hour"
		}
	}
}

private fun Date.getWeekDayString(context: Context): String {
	val weekDay = SimpleDateFormat("EEE", Locale.ENGLISH).format(this).toLowerCase()

	return when (weekDay) {
		"mon" -> context.getString(R.string.mon)
		"tue" -> context.getString(R.string.tue)
		"wed" -> context.getString(R.string.wed)
		"thu" -> context.getString(R.string.thu)
		"fri" -> context.getString(R.string.fri)
		"sat" -> context.getString(R.string.sat)
		"sun" -> context.getString(R.string.sun)
		else -> ""
	}
}

private fun Date.getMonthString(context: Context): String {
	val month = SimpleDateFormat("MMM", Locale.ENGLISH).format(this).toLowerCase()

	return when (month) {
		"jan" -> context.getString(R.string.jan)
		"feb" -> context.getString(R.string.feb)
		"mar" -> context.getString(R.string.mar)
		"apr" -> context.getString(R.string.apr)
		"may" -> context.getString(R.string.may)
		"jun" -> context.getString(R.string.jun)
		"jul" -> context.getString(R.string.jul)
		"aug" -> context.getString(R.string.aug)
		"sep" -> context.getString(R.string.sep)
		"oct" -> context.getString(R.string.oct)
		"nov" -> context.getString(R.string.nov)
		"dec" -> context.getString(R.string.dec)
		else -> ""
	}
}

private fun Date.getLocalizedTime(context: Context): String {
	val meridian = SimpleDateFormat("aa", Locale.ENGLISH).format(this)
	val hour = SimpleDateFormat("hh", Locale.ENGLISH).format(this).toInt()
	val minute = SimpleDateFormat("mm", Locale.ENGLISH).format(this).toInt()
	val leadedMinute = String.format("%02d", minute)

	val res = context.resources
	val conf = res.configuration

	if (conf.locale.toString() !in listOf(
			LOCALE_AM,
			"${LOCALE_AM}_$REGION_MASCULINE",
			"${LOCALE_AM}_$REGION_FEMININE"
		)
	) {
		return "$hour:$leadedMinute $meridian"
	}

	val amharicHour = when {
		hour > 6 -> hour - 6
		else -> hour + 6
	}

	val amharicMeridian = when (meridian) {
		"pm" -> {
			when (amharicHour) {
				in 6..11 -> "ከቀኑ"
				else -> "ከምሽቱ"
			}
		}
		else -> {
			when (amharicHour) {
				in 6..11 -> "ከለሊቱ"
				else -> "ከጠዋቱ"
			}
		}
	}

	return "$amharicMeridian $amharicHour:$leadedMinute"
}