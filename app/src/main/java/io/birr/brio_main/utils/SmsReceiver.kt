package io.birr.brio_main.utils

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.app.NotificationCompat
import android.support.v7.app.AppCompatActivity
import android.telephony.SmsMessage
import android.util.Log
import io.birr.brio_main.R
import io.birr.brio_main.ui.activities.MainActivity

class SmsReceiver : BroadcastReceiver() {
	private val cbeAddress = "CBE Birr"

	override fun onReceive(context: Context, intent: Intent) {
		val bundle = intent.extras

		try {
			bundle ?: return

			val pdusObj = bundle.get("pdus") as Array<ByteArray>

			val sms = pdusObj.map {
				val currentMsg = SmsMessage.createFromPdu(it)
//				val senderNum = currentMsg.displayOriginatingAddress

				val msg = currentMsg.displayMessageBody
				msg

			}.reduce { acc, b ->
				acc + b
			}

			val implicitBalanceRegex = ResponsePatterns.IMPLICIT_BALANCE.toRegex()
			val sendMoneyRegex = ResponsePatterns.SEND_MONEY.toRegex()
			val receiveMoneyRegex = ResponsePatterns.RECEIVE_MONEY.toRegex()
			val buyAirtimeRegex = ResponsePatterns.BUY_AIRTIME.toRegex()

			when {

				sms.matches(sendMoneyRegex) -> {
					val moneyAmount = sms.replace(sendMoneyRegex, {
						it.groupValues[1]
					})

					val receiver = sms.replace(sendMoneyRegex, {
						it.groupValues[2]
					})

					val balance = sms.replace(implicitBalanceRegex, {
						it.groupValues[1]
					})

					context.createNotif(
						context.getString(R.string.sent_money_notif_ticker),
						context.getString(R.string.sent_money_notif_ticker),
						String.format(context.getString(R.string.sent_money_notif), moneyAmount, receiver, balance)
					)
				}

				sms.matches(receiveMoneyRegex) -> {
					val moneyAmount = sms.replace(receiveMoneyRegex, {
						it.groupValues[1]
					})

					val sender = sms.replace(receiveMoneyRegex, {
						it.groupValues[2]
					})

					val balance = sms.replace(implicitBalanceRegex, {
						it.groupValues[1]
					})

					context.createNotif(
						context.getString(R.string.received_money_notif_ticker),
						context.getString(R.string.received_money_notif_ticker),
						String.format(context.getString(R.string.received_money_notif), moneyAmount, sender, balance)
					)
				}

				sms.matches(buyAirtimeRegex) -> {
					val moneyAmount = sms.replace(buyAirtimeRegex, {
						it.groupValues[1]
					})

					val receiver = sms.replace(buyAirtimeRegex, {
						it.groupValues[2]
					})

					val balance = sms.replace(implicitBalanceRegex, {
						it.groupValues[1]
					})

					context.createNotif(
						context.getString(R.string.buy_airtime_notif_ticker),
						context.getString(R.string.buy_airtime_notif_ticker),
						String.format(context.getString(R.string.buy_airtime_notif), moneyAmount, receiver, balance)
					)
				}
			}

			smsMarkAsRead(context)
//					abortBroadcast()
		} catch (error: Exception) {
			Log.e("Error", error.message)
		}
	}

	private fun Context.createNotif(ticker: String, title: String, content: String) {
		val notif = NotificationCompat.Builder(this)

		notif.setAutoCancel(true)
		notif.setSmallIcon(R.drawable.logo_cbe)
		notif.setTicker(ticker)
		notif.setWhen(System.currentTimeMillis())
		notif.setContentTitle(title)
		notif.setContentText(content)

		val intent = Intent(this, MainActivity::class.java)
		val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
		notif.setContentIntent(pendingIntent)

		val notifMan = this.getSystemService(AppCompatActivity.NOTIFICATION_SERVICE) as NotificationManager
		notifMan.notify(1234, notif.build())
	}

	private fun smsMarkAsRead(context: Context) {
		val uri = Uri.parse("content://sms/inbox")
		val cur = context.contentResolver.query(uri, null, null, null, null)

		try {

			while (cur.moveToNext()) {
				if (
					cur.getString(cur.getColumnIndex("address")) == cbeAddress &&
					cur.getInt(cur.getColumnIndex("read")) == 0
				) {
					val smsMessageId = cur.getString(cur.getColumnIndex("_id"))
					val values = ContentValues()
					values.put("read", true)

					context.contentResolver.update(Uri.parse("content://sms/inbox"), values, "_id=$smsMessageId", null)
					return
				}
			}
		} catch (e: Exception) {
			Log.e("Mark Read", "Error in Read: " + e.toString())
		} finally {
			cur.close()
		}

	}
}
