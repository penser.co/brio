package io.birr.brio_main.utils.remoteConfig

object RemoteConfigKeys {
	val BRIO_VERIFIER_NUMBER_KEY = "brio_verifier_number"
	val CBE_BOUGHT_AIRTIME_MSG_PATTERN_KEY = "cbe_bought_aritime_msg_pattern"
}
