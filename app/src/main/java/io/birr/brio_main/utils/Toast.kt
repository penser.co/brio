package io.birr.brio_main.utils

import android.app.Activity
import android.os.Build
import android.util.Log
import android.view.Gravity
import android.widget.TextView
import android.widget.Toast
import io.birr.brio_main.BuildConfig
import io.birr.brio_main.R
import io.birr.brio_main.utils.UI.tint
import kotlinx.android.synthetic.main.custom_toast.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.findOptional
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.textColor


fun Activity.debug(msg: String, type: String = "debug") {
	if (BuildConfig.DEBUG) {
		toast(msg, Toast.LENGTH_LONG, type)
	}
}

fun Activity.toast(stringId: Int, duration: Int = Toast.LENGTH_SHORT, type: String = "info") {
	try {
		toast(getString(stringId), duration, type)
	} catch (e: Exception) {
		Log.e("BRIO Activity", e.message)
	}
}

fun Activity.toast(msg: String, duration: Int = Toast.LENGTH_SHORT, type: String = "info") {
	val layout = layoutInflater.inflate(R.layout.custom_toast, customToastLayout)

	layout.findOptional<TextView>(R.id.customToastText)?.apply {
		text = msg

		textColor = color(when (type) {
			"info" -> R.color.gray50
			"debug" -> R.color.gray50
			else -> R.color.text900
		})
	}

	layout.apply {
		val color = when (type) {
			"error" -> R.color.pink
			"success" -> R.color.green
			"debug" -> R.color.text900
			else -> R.color.primary500
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			tint(color)
			horizontalPadding = 24
		} else {
			backgroundColor = color(color)
		}
	}

	Toast(this).apply {
		setGravity(Gravity.BOTTOM, 0, 350)
		setDuration(duration)
		view = layout
	}.show()
}
