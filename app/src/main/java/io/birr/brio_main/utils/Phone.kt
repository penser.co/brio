package io.birr.brio_main.utils

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.telephony.SubscriptionManager
import android.util.Log
import io.birr.brio_main.model.getPrefInt
import org.jetbrains.anko.doAsync

object USSD {
	const val CBE_NUM = 847

	fun makeQueryFromList(args: List<Any>): String {
		val q = args.map { it.toString() }.reduce { acc, s ->
			"$acc*$s"
		}

		return "*$CBE_NUM*$q#"
	}

	fun makeQuery(vararg args: Any): String {
		return makeQueryFromList(args.toList())
	}
}

object Sim {
	val SLOTS = arrayOf("extra_asus_dial_use_dualsim", "com.android.number.extra.slot", "slot", "simslot", "sim_slot", "subscription", "Subscription", "number", "com.android.number.DialingMode", "simSlot", "slot_id", "simId", "simnum", "phone_type", "slotId", "slotIdx")
}

fun Activity.cbeUssdCall(vararg args: Any) {
	val ussd = USSD.makeQueryFromList(args.toList())
	ussdCall(ussd)
}

fun Activity.ussdCall(num: String) {
	withPermission(Manifest.permission.CALL_PHONE) {
		if (Build.VERSION.SDK_INT >= 22) {
			val simList = getSimList()
			val sim = getPrefInt("selected_sim")

			doAsync {
				val intent = Intent(Intent.ACTION_CALL, Uri.fromParts("tel", num, null)).apply {
					if (simList != null && simList.size > 1) {
						flags = Intent.FLAG_ACTIVITY_NEW_TASK
						Sim.SLOTS.forEach { putExtra(it, sim) }
						putExtra("com.android.number.force.slot", true)
						putExtra("Cdma_Supp", true)
					}
				}

				startActivity(intent)
			}
		} else {
			try {
				val intent = Intent(Intent.ACTION_CALL)
				intent.data = Uri.fromParts("tel", num, null)

				startActivityForResult(intent, 1)
			} catch (e: SecurityException) {
				Log.e("BrioActivity", e.message)
			}
		}
	}
}

fun Activity.getSimList(): List<Int>? {
	return if (Build.VERSION.SDK_INT >= 22) {
		SubscriptionManager
			.from(this)
			.activeSubscriptionInfoList
			.map {
				it.subscriptionId
			}
	} else null // <<--- TODO: use TelephonyInfo for this
}
