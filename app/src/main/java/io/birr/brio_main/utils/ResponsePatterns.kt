package io.birr.brio_main.utils

import java.util.regex.Pattern

object ResponsePatterns {

	val BUY_AIRTIME: Pattern = Pattern.compile("""(?i)^dear[\s]*customer[\s]*,[\s]*you[\s]*bought[\s]*([0-9]+(?:\.[0-9]+)?)[\s]*br[.]?[\s]*of[\s]*airtime[\s]*for[\s]*(?:\+2519|2519|09|9)([0-9]{8})(?:.)*$""")

	val SEND_MONEY: Pattern = Pattern.compile("""(?i)^dear[\s]*customer[\s]*,[\s]*you[\s]*sent[\s]*([0-9]+(?:\.[0-9]+)?)[\s]*br[.]?[\s]*to[\s]*(.+)[\s]*on[\s]*(?:[0-9]+\/[0-9]+\/[0-9]+)(?:.)*$""")

	val RECEIVE_MONEY: Pattern = Pattern.compile("""(?i)^dear[\s]*customer[\s]*,[\s]*you[\s]*received[\s]*([0-9]+(?:\.[0-9]+)?)[\s]*br[.]?[\s]*from[\s]*(?:\+2519|2519|09|9)([0-9]{8})[\s]*-[\s]*([a-z0-9\s]+)*[\s]*on[\s]*[0-9]{2}/[0-9]{2}/[0-9]{2}(?:.)*$""")

	val BALANCE_CREDIT: Pattern = Pattern.compile("""(?i)^dear[\s]*customer[\s]*,[\s]*your[\s]*cbe[\s]*birr[\s]*account[\s]*has[\s]*been[\s]*credited[\s]*with[\s]*([0-9]+(?:\.[0-9]+)?)[\s]*br[.]?(?:.)*$""")

	val BALANCE_WITHDRAWAL: Pattern = Pattern.compile("""(?i)^dear[\s]*customer[\s]*,[\s]*you[\s]*have[\s]*withdrawn[\s]*([0-9]+(?:\.[0-9]+)?)[\s]*br[.]?[\s]*from[\s]*([0-9]+)[\s]*-[\s]*([a-z\s]+)[\s]*on(?:.)*$""")

	val MAKE_PAYMENT: Pattern = Pattern.compile("""(?i)(?:.)*you[\s]*paid[\s]*([0-9]+(?:\.[0-9]+)?)[\s]*br[.]?[\s]*to[\s]*([0-9]+)[\s]*-[\s]*([a-z\s]+)[\s]*for[\s]*buying[\s]*products[\s]*on(?:.)*$""")

	val CHECK_BALANCE: Pattern = Pattern.compile("""(?i)(?:.)*your[\s]*cbe[\s]*birr[\s]*account[\s]*balance[\s]*is[\s]*([0-9]+(?:\.[0-9]+)?)[\s]*br[.]?(?:.)*$""")

	val IMPLICIT_BALANCE: Pattern = Pattern.compile("""(?i)(?:.)*your[\s]*(?:current|(?:cbe[\s]*birr[\s]*account))?[\s]*balance[\s]*is[\s]*([0-9]+(?:\.[0-9]+)?)[\s]*br[.]?(?:.)*$""")

	val WRONG_PIN: Pattern = Pattern.compile("""(?i)(?:.)*sorry[\s]*,[\s]*you[\s]*have[\s]*entered[\s]*the[\s]*wrong[\s]*pin(?:.)*$""")

	val PIN_CHANGED: Pattern = Pattern.compile("""(?i)(?:.)*dear[\s]*cbe[\s]*birr[\s]*user[\s]*,[\s]*your[\s]*pin[\s]*code[\s]*has[\s]*been[\s]*successfully[\s]*changed(?:.)*$""")

	val ACCOUNT_INIT: Pattern = Pattern.compile("""(?i)Dear[\s]*Customer[\s]*,[\s]*your[\s]*PIN[\s]*is[\s]*([0-9]{4})[\s]*.[\s]*Please[\s]*dial[\s]*\*847#[\s]*to[\s]*activate[\s]*your[\s]*CBE[\s]*Birr[\s]*account[\s]*.[\s]*Call[\s]*951[\s]*for[\s]*help[\s]*.[\s]*Thank[\s]*you!""")

	val ACCOUNT_ACTIVATED: Pattern = Pattern.compile("""(?i)(?:.)*your[\s]*cbe[\s]*birr[\s]*account[\s]*has[\s]*been[\s]*successfully[\s]*activated[\s]*.[\s]*Your[\s]*secret[\s]*word[\s]*is[\s]*([a-zA-Z0-9]{6})[\s]*.[\s]*thank[\s]*you!$""")

	val INIT_PIN: Pattern = Pattern.compile("""(?i)(?:.)*your[\s]*pin[\s]*has[\s]*been[\s]*reset[\s]*to[\s]*([0-9]{4}).[\s]*please[\s]*change[\s]*your[\s]*pin[\s]*.[\s]*thank[\s]*you!$""")

	const val INFO = "INFO"
	const val TXN = "TXN"
	const val NOT_PARSED = "NOT_PARSED"

	val BUY_AIRTIME_FLAG = Pair(TXN, "BUY_AIRTIME")
	val SEND_MONEY_FLAG = Pair(TXN, "SEND_MONEY")
	val RECEIVE_MONEY_FLAG = Pair(TXN, "RECEIVE_MONEY")
	val BALANCE_CREDIT_FLAG = Pair(TXN, "BALANCE_CREDIT")
	val BALANCE_WITHDRAWAL_FLAG = Pair(TXN, "BALANCE_WITHDRAWAL")
	val MAKE_PAYMENT_FLAG = Pair(TXN, "MAKE_PAYMENT")
	val RECEIVE_AIRTIME_FLAG = Pair(INFO, "RECEIVE_AIRTIME")
	val CHECK_BALANCE_FLAG = Pair(INFO, "CHCEK_BALANCE")
	val IMPLICIT_BALANCE_FLAG = Pair(INFO, "IMPLICIT_BALANCE")
	val NOT_PARSED_FLAG = Pair(NOT_PARSED, NOT_PARSED)
}
