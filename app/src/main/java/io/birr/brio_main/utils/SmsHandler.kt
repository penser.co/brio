package io.birr.brio_main.utils

import android.content.Context
import android.net.Uri
import io.birr.brio_main.utils.ResponsePatterns.BALANCE_CREDIT
import io.birr.brio_main.utils.ResponsePatterns.BALANCE_CREDIT_FLAG
import io.birr.brio_main.utils.ResponsePatterns.BALANCE_WITHDRAWAL
import io.birr.brio_main.utils.ResponsePatterns.BALANCE_WITHDRAWAL_FLAG
import io.birr.brio_main.utils.ResponsePatterns.BUY_AIRTIME
import io.birr.brio_main.utils.ResponsePatterns.BUY_AIRTIME_FLAG
import io.birr.brio_main.utils.ResponsePatterns.CHECK_BALANCE
import io.birr.brio_main.utils.ResponsePatterns.CHECK_BALANCE_FLAG
import io.birr.brio_main.utils.ResponsePatterns.IMPLICIT_BALANCE
import io.birr.brio_main.utils.ResponsePatterns.IMPLICIT_BALANCE_FLAG
import io.birr.brio_main.utils.ResponsePatterns.MAKE_PAYMENT
import io.birr.brio_main.utils.ResponsePatterns.MAKE_PAYMENT_FLAG
import io.birr.brio_main.utils.ResponsePatterns.NOT_PARSED_FLAG
import io.birr.brio_main.utils.ResponsePatterns.RECEIVE_MONEY
import io.birr.brio_main.utils.ResponsePatterns.RECEIVE_MONEY_FLAG
import io.birr.brio_main.utils.ResponsePatterns.SEND_MONEY
import io.birr.brio_main.utils.ResponsePatterns.SEND_MONEY_FLAG

object SmsHandler {

	private fun getSmsType(sms: String): Pair<String, String> {
		return when {
			(sms.matches(BUY_AIRTIME.toRegex())) -> BUY_AIRTIME_FLAG
			(sms.matches(SEND_MONEY.toRegex())) -> SEND_MONEY_FLAG
			(sms.matches(BALANCE_CREDIT.toRegex())) -> BALANCE_CREDIT_FLAG
			(sms.matches(BALANCE_WITHDRAWAL.toRegex())) -> BALANCE_WITHDRAWAL_FLAG
			(sms.matches(MAKE_PAYMENT.toRegex())) -> MAKE_PAYMENT_FLAG
			(sms.matches(RECEIVE_MONEY.toRegex())) -> RECEIVE_MONEY_FLAG
			(sms.matches(CHECK_BALANCE.toRegex())) -> CHECK_BALANCE_FLAG
			(sms.matches(IMPLICIT_BALANCE.toRegex())) -> IMPLICIT_BALANCE_FLAG
			else -> NOT_PARSED_FLAG
		}
	}

	fun isImplicitBalance(sms: String): Boolean {
		return sms.matches(ResponsePatterns.IMPLICIT_BALANCE.toRegex())
	}

	fun getCbeSmsList(context: Context): List<List<Pair<String, String>>> {
		return getSmsList(context, "cbe birr")
//		return getSmsList(context, "+251904183553")
	}

	fun getBrioSmsList(context: Context): List<List<Pair<String, String>>> {
		return getSmsList(context, "+251904183553")
	}

	private fun getSmsList(context: Context, sender: String): List<List<Pair<String, String>>> {
		val smsList = mutableListOf<List<Pair<String, String>>>()

		val cur = context.contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null)

		cur ?: return listOf()

		if (cur.moveToFirst()) { // must check the result to prevent exception
			do {
				val msgDict = mutableListOf<Pair<String, String>>()

				for (i in 0 until cur.columnCount) {
					msgDict.add(Pair(cur.getColumnName(i), cur.getString(i)))
				}

				val address = msgDict.toList().find { it.first == "address" }?.second?.toLowerCase()

				if (address == sender) {
					smsList.add(msgDict)
				}

			} while (cur.moveToNext())
		}

		cur.close()

		return smsList.toList()
	}

	fun isTxn(sms: List<Pair<String, String>>): Boolean {
		val body = sms.find {
			it.first == "body"
		}?.second

		body ?: return false
		return getSmsType(body).first == ResponsePatterns.TXN
	}
}
