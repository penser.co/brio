package io.birr.brio_main.utils

import android.content.*

fun ContextWrapper.broadcast(action: String) {
	broadcast(action, extras = null)
}

fun ContextWrapper.broadcast(action: String, extras: List<Pair<String, String>>? = null) {
	extras?.forEach {
		broadcast(action, it)
	}
}

fun ContextWrapper.broadcast(action: String, extra: Pair<String, String>? = null) {
	Intent("io.birr.brio_main.$action").apply {
		extra?.let {
			putExtra(it.first, it.second)
		}

		sendBroadcast(this)
	}
}

fun Context.onBroadcast(action: String, extras: List<String>, callback: (extras: List<Pair<String, String>>) -> Any) {

	val broadcast = IntentFilter("io.birr.brio_main.$action")

	val receiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context, intent: Intent) {
			callback(extras.map { Pair(it, intent.getStringExtra(it)) })
		}
	}

	registerReceiver(receiver, broadcast)
}
