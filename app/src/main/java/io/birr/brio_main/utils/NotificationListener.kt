package io.birr.brio_main.utils

import android.content.Intent
import android.os.Build
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification

class NotificationListener : NotificationListenerService() {

	override fun onNotificationRemoved(sbn: StatusBarNotification?) {
		super.onNotificationRemoved(sbn)
	}

	override fun onNotificationPosted(sbn: StatusBarNotification) {
		super.onNotificationPosted(sbn)

		sbn.notification?.let { notif ->
			val title = (notif.extras["android.title"] ?: "").toString()
			val text = (notif.extras["android.text"] ?: "").toString()

			if (
				notif.category == "msg" &&
				title.trim().matches("""(?i).*cbe.*birr.*""".toRegex())
			) {
				val intent = Intent("io.birr.brio_main.CBE_SMS")
				intent.putExtra("notif.title", title)
				intent.putExtra("notif.text", text)

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
					cancelNotification(sbn.key)
				}

				sendBroadcast(intent)
			}
		}
	}

}