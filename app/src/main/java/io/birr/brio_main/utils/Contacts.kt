package io.birr.brio_main.utils

import android.content.ContentProviderOperation
import android.content.Context
import android.provider.ContactsContract

data class Contact(val name: String, val number: String)

fun Context.getContacts(): List<Contact> {
	val contactsList = mutableListOf<Contact>()

	val cr = contentResolver

	val cur = cr.query(
		ContactsContract.Contacts.CONTENT_URI,
		null, null, null,
		ContactsContract.Contacts.DISPLAY_NAME + " ASC"
	)

	if (cur == null || cur.count == 0) return listOf()

	while (cur.moveToNext()) {

		val id = cur.getString(
			cur.getColumnIndex(
				ContactsContract.Contacts._ID
			)
		)

		val name = cur.getString(
			cur.getColumnIndex(
				ContactsContract.Contacts.DISPLAY_NAME
			)
		) ?: "NO NAME"

		if (
			cur.getInt(cur.getColumnIndex(
				ContactsContract.Contacts.HAS_PHONE_NUMBER)
			) > 0
		) {
			val pCur = cr.query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
				null,
				ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
				arrayOf(id),
				null
			)

			while (pCur.moveToNext()) {
				val phoneNo = pCur.getString(
					pCur.getColumnIndex(
						ContactsContract.CommonDataKinds.Phone.NUMBER
					)
				)

				val cleanedPhoneNo = phoneNo.replace("""[\s\-]""".toRegex(), "")

				if (cleanedPhoneNo.matches("""^(\+251|251|0)9[0-9]{8}$""".toRegex())) {
					val phoneNoShort = cleanedPhoneNo.replace("""^(\+251|251|0)""".toRegex(), "")
					val spaceRegex = Regex("^9([0-9])([0-9]{3})([0-9]{4})")
					val formttedContact = phoneNoShort.replace(spaceRegex, {
						"09${it.groupValues[1]} ${it.groupValues[2]} ${it.groupValues[3]}"
					})

					contactsList.add(Contact(name, formttedContact))
				}
			}

			pCur.close()
		}
	}

	cur.close()

	return contactsList
}

fun Context.saveBrioContact() {

	val displayName = "BRIO"
	val mobileNumber = "+251904183553"
//	val workNumber = "+251904183553"
	val emailID = "hello@birr.io"
	val company = "Brio FinTech"

	val ops = ArrayList<ContentProviderOperation>().apply {

		add(ContentProviderOperation.newInsert(
			ContactsContract.RawContacts.CONTENT_URI)
			.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
			.withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
			.build())

		add(ContentProviderOperation.newInsert(
			ContactsContract.Data.CONTENT_URI)
			.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
			.withValue(ContactsContract.Data.MIMETYPE,
				ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
			.withValue(
				ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
				displayName).build())

		add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
			.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
			.withValue(ContactsContract.Data.MIMETYPE,
				ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
			.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, mobileNumber)
			.withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
				ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
			.build())

		add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
			.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
			.withValue(ContactsContract.Data.MIMETYPE,
				ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
			.withValue(ContactsContract.CommonDataKinds.Email.DATA, emailID)
			.withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
			.build())

		add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
			.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
			.withValue(ContactsContract.Data.MIMETYPE,
				ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)
			.withValue(ContactsContract.CommonDataKinds.Organization.COMPANY, company)
			.withValue(ContactsContract.CommonDataKinds.Organization.TYPE, ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
			.withValue(ContactsContract.CommonDataKinds.Organization.TYPE, ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
			.build())


	}

	try {
		contentResolver.applyBatch(ContactsContract.AUTHORITY, ops)
	} catch (e: Exception) {
		e.printStackTrace()
	}
}