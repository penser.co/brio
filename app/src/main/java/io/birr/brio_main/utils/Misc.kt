package io.birr.brio_main.utils

import android.content.Context
import android.content.Context.VIBRATOR_SERVICE
import android.os.Vibrator
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import io.birr.brio_main.BuildConfig
import java.util.*

fun Context.getDeviceUuid(): String? {
	val tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

	val tmDevice: String
	val tmSerial: String
	val androidId: String

	return try {
		tmDevice = tm.deviceId
		tmSerial = tm.simSerialNumber
		androidId = android.provider.Settings.Secure.getString(contentResolver, android.provider.Settings.Secure.ANDROID_ID)

		val deviceUuid = UUID(androidId.hashCode().toLong(), tmDevice.hashCode().toLong() shl 32 or tmSerial.hashCode().toLong())

		deviceUuid.toString()
	} catch (e: SecurityException) {
		Log.e("TransactionActivity", e.message)
		null
	}
}

fun Context.vibrate() {
	val vibrator = getSystemService(VIBRATOR_SERVICE) as Vibrator
	vibrator.vibrate(200)
}

fun Context.isAirplaneModeOn(): Boolean {
	return Settings.System.getInt(contentResolver,
		Settings.Global.AIRPLANE_MODE_ON, 0) != 0
}

fun debug(callback: () -> String? = fun() = "No debug value") {
	if (BuildConfig.DEBUG) {
		Log.d("BRIO_DEBUG", callback())
	}
}