package io.birr.brio_main.model

import android.content.Context

object AppData {
	const val GENDER_MALE = "male"
	const val GENDER_FEMALE = "female"
}

fun Context.resetData() {
	setPref("is_tour_done", false)
}
