package io.birr.brio_main.model

import android.content.Context

object SharedPrefs {
	const val NAME = "io.birr.brio_main.prefs"
}

fun Context.setPref(key: String, value: Any) {
	getSharedPreferences(SharedPrefs.NAME, 0).apply {
		edit().apply {
			when (value) {
				is String -> putString(key, value)
				is Int -> putInt(key, value)
				is Boolean -> putBoolean(key, value)
				is Float -> putFloat(key, value)
				is Long -> putLong(key, value)
				else -> putString(key, value.toString())
			}
		}.apply()
	}
}

fun Context.getPrefString(key: String): String {
	return getSharedPreferences(SharedPrefs.NAME, 0)
		.getString(key, "")
}

fun Context.getPrefInt(key: String): Int {
	return getSharedPreferences(SharedPrefs.NAME, 0)
		.getInt(key, 0)
}

fun Context.getPrefBoolean(key: String): Boolean {
	return getSharedPreferences(SharedPrefs.NAME, 0)
		.getBoolean(key, false)
}

fun Context.getPrefFloat(key: String): Float {
	return getSharedPreferences(SharedPrefs.NAME, 0)
		.getFloat(key, 0f)
}

fun Context.getPrefLong(key: String): Long {
	return getSharedPreferences(SharedPrefs.NAME, 0)
		.getLong(key, 0)
}
