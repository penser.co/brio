package io.birr.brio_main.ui.fragments

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import io.birr.brio_main.R
import io.birr.brio_main.utils.onTextChanged
import kotlinx.android.synthetic.main.fragment_sign_up_names.*
import org.jetbrains.anko.findOptional
import org.jetbrains.anko.imageResource

class SignUpNamesFragment : Fragment() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
	}

	override fun onAttach(context: Context?) {
		super.onAttach(context)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		val view = inflater.inflate(R.layout.fragment_sign_up_names, container, false)

		return view
	}

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)

		firstNameInput.onTextChanged { updateNamesFormState() }
		middleNameInput.onTextChanged { updateNamesFormState() }
		lastNameInput.onTextChanged { updateNamesFormState() }
	}

	private fun updateNamesFormState() {
		val firstName = findOptional<EditText>(R.id.firstNameInput)?.text.toString()
		val middleName = findOptional<EditText>(R.id.middleNameInput)?.text.toString()
		val lastName = findOptional<EditText>(R.id.lastNameInput)?.text.toString()

		val submitIcon = findOptional<ImageView>(R.id.signUpNamesSubmitIcon)

		submitIcon?.imageResource = if (
			firstName.isEmpty() || middleName.isEmpty() || lastName.isEmpty()
		) R.drawable.ic_ok_gray else R.drawable.ic_ok_green
	}
}
