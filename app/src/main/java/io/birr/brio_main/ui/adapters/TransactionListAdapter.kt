package io.birr.brio_main.ui.adapters

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.provider.BaseColumns
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import io.birr.brio_main.R
import io.birr.brio_main.utils.ResponsePatterns.BALANCE_CREDIT
import io.birr.brio_main.utils.ResponsePatterns.BALANCE_CREDIT_FLAG
import io.birr.brio_main.utils.ResponsePatterns.BALANCE_WITHDRAWAL
import io.birr.brio_main.utils.ResponsePatterns.BALANCE_WITHDRAWAL_FLAG
import io.birr.brio_main.utils.ResponsePatterns.BUY_AIRTIME
import io.birr.brio_main.utils.ResponsePatterns.BUY_AIRTIME_FLAG
import io.birr.brio_main.utils.ResponsePatterns.MAKE_PAYMENT
import io.birr.brio_main.utils.ResponsePatterns.MAKE_PAYMENT_FLAG
import io.birr.brio_main.utils.ResponsePatterns.RECEIVE_MONEY
import io.birr.brio_main.utils.ResponsePatterns.RECEIVE_MONEY_FLAG
import io.birr.brio_main.utils.ResponsePatterns.SEND_MONEY
import io.birr.brio_main.utils.ResponsePatterns.SEND_MONEY_FLAG
import io.birr.brio_main.utils.color
import io.birr.brio_main.utils.formatDate
import org.jetbrains.anko.find
import org.jetbrains.anko.textColor
import java.util.*

class TransactionListAdapter(
	val context: Context,
	private val txns: List<List<Pair<String, String>>>
) : BaseAdapter() {
	val _remoteConfig = FirebaseRemoteConfig.getInstance()

	override fun getCount(): Int = txns.size

	override fun getItemId(position: Int): Long = position.toLong()

	override fun getItem(position: Int): Any = ""

	override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
		val item = txns[position]

		val inflater = context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
		val view = inflater.inflate(R.layout.list_item_transaction, null)

		val body = item.find { it.first == "body" }?.second
		body ?: return view

		val timeView = view.find<TextView>(R.id.transactionTime)
		val amountView = view.find<TextView>(R.id.transactionAmount)
		val amountSignView = view.find<TextView>(R.id.transactionAmountSign)
		val subjectView = view.find<TextView>(R.id.transactionPerson)
		val descView = view.find<TextView>(R.id.transactionDesc)

		val dateSent = item.find { it.first == "date" }?.second
		val date = Date((dateSent ?: "").toLong())
		val formattedDate = date.formatDate(context)

		timeView.text = formattedDate

		val txn = getTxn(body)

		val amount = txn.find { it.first == "amount" }?.second
		val desc = txn.find { it.first == "desc" }?.second
		val subject = txn.find { it.first == "subject" }?.second
		val subjectType = txn.find { it.first == "subject_type" }?.second
		val sign = txn.find { it.first == "sign" }?.second

		val color = when (sign) {
			"-" -> R.color.pink
			"+" -> R.color.green
			else -> R.color.gray200
		}

		amountView.text = amount
//		amountSignView.text = sign
		descView.text = desc

		subjectView.text = when (subjectType) {
			"from" -> String.format(context.getString(R.string.from_contact), subject)
			"for" -> String.format(context.getString(R.string.for_contact), subject)
			"to" -> String.format(context.getString(R.string.to_contact), subject)
			else -> subject
		}

		if (subject == null) subjectView.visibility = View.GONE

		amountView.textColor = context.color(color)
		amountSignView.textColor = context.color(color)

		return view
	}

	private fun formatTxn(type: String?, desc: String?, amount: String?, sign: String?, subject: String?, subjectType: String?): List<Pair<String, String?>> {

		return listOf(
			Pair("type", type),
			Pair("desc", desc),
			Pair("amount", amount),
			Pair("sign", sign),
			Pair("subject", subject),
			Pair("subject_type", subjectType)
		)
	}

	private fun getTxn(msg: String): List<Pair<String, String?>> {

//		val buyAirtimeRegex = getRemoteRegex(CBE_BOUGHT_AIRTIME_MSG_PATTERN_KEY)
		val buyAirtimeRegex = BUY_AIRTIME.toRegex()
		val sendMoneyRegex = SEND_MONEY.toRegex()
		val receiveMoneyRegex = RECEIVE_MONEY.toRegex()
		val balanceCreditRegex = BALANCE_CREDIT.toRegex()
		val balanceWithdrawalRegex = BALANCE_WITHDRAWAL.toRegex()
		val makePaymentRegex = MAKE_PAYMENT.toRegex()

		return when {
			msg.matches(buyAirtimeRegex) -> {

				val amount = msg.replace(buyAirtimeRegex, {
					it.groupValues[1]
				})

				val subjectRaw = msg.replace(buyAirtimeRegex, {
					it.groupValues[2]
				})

				val spacingRegex = Regex("^([0-9])([0-9]{3})([0-9]{4})")
				val subject = subjectRaw.replace(spacingRegex, {
					"09${it.groupValues[1]} ${it.groupValues[2]} ${it.groupValues[3]}"
				})

				val desc = context.getString(R.string.bought_mobile_airtime)

				formatTxn(BUY_AIRTIME_FLAG.second, desc, amount, "-", subject, "for")
			}

			msg.matches(sendMoneyRegex) -> {

				val amount = msg.replace(sendMoneyRegex, {
					it.groupValues[1]
				})

				val subject = msg.replace(sendMoneyRegex, {
					it.groupValues[2]
				})

				val desc = context.getString(R.string.sent_money)

				formatTxn(SEND_MONEY_FLAG.second, desc, amount, "-", subject, "to")
			}

			msg.matches(receiveMoneyRegex) -> {

				val amount = msg.replace(receiveMoneyRegex, {
					it.groupValues[1]
				})

				val subjectNum = msg.replace(receiveMoneyRegex, {
					it.groupValues[2]
				})

				val subjectName = msg.replace(receiveMoneyRegex, {
					it.groupValues[3].split(" ").let {
						"${it[0]} ${it[1]}"
					}
				})

				val subject = subjectName
				val spacingRegex = Regex("^([0-9])([0-9]{3})([0-9]{4})")
//				val subject = subjectNum.replace(spacingRegex, {
//					"09${it.groupValues[1]} ${it.groupValues[2]} ${it.groupValues[3]}"
//				})

				val desc = context.getString(R.string.received_money)

				formatTxn(RECEIVE_MONEY_FLAG.second, desc, amount, "+", subject, "from")
			}

			msg.matches(balanceCreditRegex) -> {
				val amount = msg.replace(balanceCreditRegex, {
					it.groupValues[1]
				})

				val desc = context.getString(R.string.balance_credited)

				formatTxn(BALANCE_CREDIT_FLAG.second, desc, amount, "+", null, null)
			}

			msg.matches(balanceWithdrawalRegex) -> {
				val amount = msg.replace(balanceWithdrawalRegex, {
					it.groupValues[1]
				})

				val subject = msg.replace(balanceWithdrawalRegex, {
					"${it.groupValues[3]} • ${it.groupValues[2]}"
				})

				val desc = context.getString(R.string.balance_withdrawn)

				formatTxn(BALANCE_WITHDRAWAL_FLAG.second, desc, amount, "-", subject, "from")
			}

			msg.matches(makePaymentRegex) -> {
				val amount = msg.replace(makePaymentRegex, {
					it.groupValues[1]
				})

				val subject = msg.replace(makePaymentRegex, {
					"${it.groupValues[3]} • ${it.groupValues[2]}"
				})

				val desc = context.getString(R.string.made_payment)

				formatTxn(MAKE_PAYMENT_FLAG.second, desc, amount, "-", subject, "to")
			}

			else -> {
				formatTxn(null, null, null, null, null, null)
			}
		}
	}

	fun getContactDisplayNameByNumber(number: String): String {
		val uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number))
		var name = "?"

		val contentResolver = context.contentResolver
		val contactLookup = contentResolver.query(uri, arrayOf(BaseColumns._ID, ContactsContract.PhoneLookup.DISPLAY_NAME), null, null, null)

		try {
			if (contactLookup != null && contactLookup.count > 0) {
				contactLookup.moveToNext()
				name = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.DISPLAY_NAME))
				//String contactId = contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID));
			}
		} finally {
			if (contactLookup != null) {
				contactLookup.close()
			}
		}

		return name
	}
}
