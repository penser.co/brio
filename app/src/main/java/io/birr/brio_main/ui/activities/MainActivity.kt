package io.birr.brio_main.ui.activities

import android.Manifest
import android.Manifest.permission.*
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Build
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetBehavior.STATE_EXPANDED
import android.support.design.widget.BottomSheetDialog
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.telephony.TelephonyManager
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.vision.barcode.Barcode
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.zxing.Result
import io.birr.brio_main.BuildConfig
import io.birr.brio_main.R
import io.birr.brio_main.barcode.BarcodeCaptureActivity
import io.birr.brio_main.model.*
import io.birr.brio_main.model.AppData.GENDER_FEMALE
import io.birr.brio_main.model.AppData.GENDER_MALE
import io.birr.brio_main.ui.adapters.ContactsRecyclerAdapter
import io.birr.brio_main.utils.*
import io.birr.brio_main.utils.AppLocale.Companion.LOCALE_AM
import io.birr.brio_main.utils.AppLocale.Companion.LOCALE_EN
import io.birr.brio_main.utils.AppLocale.Companion.REGION_FEMININE
import io.birr.brio_main.utils.AppLocale.Companion.REGION_MASCULINE
import io.birr.brio_main.utils.SmsHandler.getBrioSmsList
import io.birr.brio_main.utils.SmsHandler.getCbeSmsList
import io.birr.brio_main.utils.remoteConfig.RemoteConfigDefaults.BRIO_VERIFIER_NUMBER_DEFAULT
import io.birr.brio_main.utils.remoteConfig.RemoteConfigKeys.BRIO_VERIFIER_NUMBER_KEY
import io.birr.brio_main.utils.remoteConfig.RemoteConfigKeys.CBE_BOUGHT_AIRTIME_MSG_PATTERN_KEY
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_sheet_contacts.*
import kotlinx.android.synthetic.main.bottom_sheet_profile.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.jetbrains.anko.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

	private val _rc = FirebaseRemoteConfig.getInstance()
	private val _rootRef = FirebaseDatabase.getInstance().reference
	private var _auth: FirebaseAuth? = null

	private lateinit var _bsDialog: BottomSheetDialog

	private var _cbeSmsList: List<List<Pair<String, String>>> = listOf()
	private var _enteredPin: String = ""
	private var _newPin: String = ""
	private var _oldPin: String = ""
	private var _pinRedirect: String? = null
	private var _moneyAmount: Int = 0
	private var _airtimeSubject: Int = 1
	private var _airtimeContact: Contact? = null
	private var _bottomSheetState: String = "buy_airtime"
	private var _tillNumber: String = ""
	private var _phoneNumber: String = ""
	private var _settingState: String = "init"
	private var _zXingScannerView: ZXingScannerView? = null
	private var _receiver: BroadcastReceiver? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		refreshLocale()

		withPermission(READ_SMS) {
			_cbeSmsList = getCbeSmsList(this@MainActivity)
		}

		_rc.setConfigSettings(
			FirebaseRemoteConfigSettings.Builder()
				.setDeveloperModeEnabled(BuildConfig.DEBUG)
				.build()
		)

		val defaults = mapOf(
			BRIO_VERIFIER_NUMBER_KEY to BRIO_VERIFIER_NUMBER_DEFAULT,
			CBE_BOUGHT_AIRTIME_MSG_PATTERN_KEY to
				"""(?i)^dear[\s]*customer[\s]*,[\s]*you[\s]*bought[\s]*([0-9]+(?:\.[0-9]+)?)[\s]*br[.]?[\s]*of[\s]*airtime[\s]*for[\s]*(?:\+2519|2519|09|9)([0-9]{8})(?:.)*$"""
		)

		_rc.setDefaults(defaults)
		_rc.fetch(0).let {
			it.addOnSuccessListener(this) {
				_rc.activateFetched()
			}
		}

//		val BUY_AIRTIME: Pattern = Pattern.compile("""(?i)^dear[\s]*customer[\s]*,[\s]*you[\s]*bought[\s]*([0-9]+(?:\.[0-9]+)?)[\s]*br[.]?[\s]*of[\s]*airtime[\s]*for[\s]*(?:\+2519|2519|09|9)([0-9]{8})(?:.)*$""")

//		val regex = _rc.getString(CBE_BOUGHT_AIRTIME_MSG_PATTERN_KEY).toRegex()
//		val txt = "Dear Customer, you bought 50.00Br. of airtime for 251944108619"

//		val service = Intent(this, CallService::class.java)
//		startService(service)

		checkPermissions(
			CALL_PHONE,
			ACCESS_COARSE_LOCATION,
			ACCESS_FINE_LOCATION,
			SEND_SMS
//			READ_SMS,
//			CAMERA,
//			READ_CONTACTS,
		)

		setContentView(R.layout.activity_main)

		_bsDialog = BottomSheetDialog(this)

		val qrScanBroadcast = IntentFilter("io.birr.brio_main.QR_SCAN_RESULT")

		_receiver = object : BroadcastReceiver() {
			override fun onReceive(context: Context, intent: Intent) {
				val res = intent.getStringExtra("scan_result")
				parseQrResult(res)
			}
		}

		registerReceiver(_receiver, qrScanBroadcast)

		val notifBroadcast = IntentFilter("io.birr.brio_main.CBE_SMS")

		val receiver = object : BroadcastReceiver() {
			override fun onReceive(context: Context, intent: Intent) {
				val text = intent.getStringExtra("notif.text")
				val title = intent.getStringExtra("notif.title")

//				createNotif("BRIO - Notification!", "SMS from CBE", text)
			}
		}

		registerReceiver(receiver, notifBroadcast)

//		checkPhoneNumVerification()
		pushToFirebase()
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		if (
			requestCode == BARCODE_READER_REQUEST_CODE &&
			resultCode == CommonStatusCodes.SUCCESS &&
			data != null
		) {
			val barcode = data.getParcelableExtra<Barcode>(BarcodeCaptureActivity.BarcodeObject)

			val intent = Intent("io.birr.brio_main.QR_SCAN_RESULT")
			intent.putExtra("scan_result", barcode.displayValue)

			sendBroadcast(intent)
		}
	}

	override fun onResume() {
		super.onResume()

		checkCbeActivation()
	}

	override fun onPause() {
		super.onPause()
		_zXingScannerView?.stopCamera()
	}

	override fun handleResult(result: Result?) {

//		toast(result?.text ?: "NO RESULT")
	}

	private fun Context.createNotif(ticker: String, title: String, content: String) {
		val notif = NotificationCompat.Builder(this)

		notif.setAutoCancel(true)
		notif.setSmallIcon(R.drawable.logo_cbe)
		notif.setTicker(ticker)
		notif.setWhen(System.currentTimeMillis())
		notif.setContentTitle(title)
		notif.setContentText(content)

		val intent = Intent(this, MainActivity::class.java)
		val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
		notif.setContentIntent(pendingIntent)

		val notifMan = this.getSystemService(AppCompatActivity.NOTIFICATION_SERVICE) as NotificationManager
		notifMan.notify(1234, notif.build())
	}

	private fun pushToFirebase() {
		doAsync {
			val contacts = getContacts()

			_auth = FirebaseAuth.getInstance()

			_auth?.let { auth ->

				auth.signInAnonymously().addOnCompleteListener({
					if (it.isSuccessful) {
						val userRef = _rootRef.child(FirebaseInstanceId.getInstance().id)
						val profileRef = userRef.child("profile")
						val nameRef = profileRef.child("name")
						val birthdateRef = profileRef.child("birthdate")
						val genderRef = profileRef.child("gender")
						val phoneNumRef = profileRef.child("phone_number")

						val name = mapOf(
							"first" to getPrefString("first_name"),
							"middle" to getPrefString("middle_name"),
							"last" to getPrefString("last_name")
						)

						val birthdate = mapOf(
							"day" to getPrefString("birth_day"),
							"month" to getPrefString("birth_month"),
							"year" to getPrefString("birth_year")
						)

						nameRef.child("first").setValue(name["first"])
						nameRef.child("middle").setValue(name["middle"])
						nameRef.child("last").setValue(name["last"])

						birthdateRef.child("day").setValue(birthdate["day"])
						birthdateRef.child("month").setValue(birthdate["month"])
						birthdateRef.child("year").setValue(birthdate["year"])

						genderRef.setValue(getPrefString("profile_gender"))

						getPrefString("phone_num").let {
							phoneNumRef.setValue(it)
						}

						contacts.forEach {
							userRef.child("contacts").child(it.number).setValue(it.name)
						}

						_cbeSmsList.let {
							it.forEach {
								val sms = it.toMap()
								val uid = "${sms["_id"]}-${sms["date"]}-${sms["date_sent"]}"
								userRef.child("cbe-raw-sms").child(uid).setValue(sms)
							}
						}
					}
				})
			}
		}
	}

	/*
	 * bottom sheet functions
	 */
	fun bottomSheetInit(v: View) {
		val tag = v.tag.toString()

		bottomSheetShow(tag)
	}

	private fun bottomSheetShow(tag: String) {
		_enteredPin = ""
		_moneyAmount = 0

		when (tag) {
			"buy_airtime" -> {
				_bottomSheetState = "buy_airtime"
				_airtimeSubject = 1
				bottomSheetAirtimeReceiverShow()
			}

			"send_money" -> {
				_bottomSheetState = "send_money"
				bottomSheetContactsShow()
			}

			"make_payment" -> {
				_bottomSheetState = "make_payment"
				bottomSheetTillShow()
			}

			"withdraw_money" -> {
				_bottomSheetState = "withdraw_money"
				bottomSheetTillShow()
			}
		}
	}

	fun bottomSheetAmountAppend(v: View) {
		val btnVal = v.tag.toString()

		val amount = _bsDialog.findOptional<TextView>(R.id.bottomSheetAmountText)
		val current = amount?.text

		current ?: return

		when (_bottomSheetState) {
			"buy_airtime" -> if (current.length >= 4) return
			"send_money" -> if (current.length >= 5) return
			"make_payment" -> if (current.length >= 5) return
			"withdraw_money" -> if (current.length >= 5) return
			else -> if (current.length >= 5) return
		}

		when (current) {
			"0" -> amount.text = btnVal
			else -> amount.text = "$current$btnVal"
		}

		bottomSheetAmountUpdate()
	}

	fun bottomSheetAmountDelete(v: View) {
		val amount = _bsDialog.findOptional<TextView>(R.id.bottomSheetAmountText)
		val current = amount?.text

		bottomSheetAmountUpdate()
		if (current == "0") return

		if (current == null || current.length == 1) {
			amount?.text = "0"
			bottomSheetAmountUpdate()

			return
		}

		amount.text = current.substring(0, current.length - 1)
		bottomSheetAmountUpdate()
	}

	private fun bottomSheetAmountUpdate() {
		_bsDialog.let {

			val submit = it.findOptional<ImageView>(R.id.airtimeAmountSubmit)
			val amount = it.findOptional<TextView>(R.id.bottomSheetAmountText)
			val currency = it.findOptional<TextView>(R.id.bottomSheetAmountCurrency)
			val delete = it.findOptional<ImageView>(R.id.bottomSheetAmountDelete)

			val green = color(R.color.green)
			val gray = color(R.color.gray200)
			val pink = color(R.color.pink)

			_moneyAmount = amount?.text.toString().toInt()

			if (isMoneyAmountValid()) {
				submit?.imageResource = R.drawable.ic_ok_green
				amount?.textColor = green
				currency?.textColor = green
			} else {
				submit?.imageResource = R.drawable.ic_ok_gray
				amount?.textColor = pink
				currency?.textColor = pink

				if (_moneyAmount == 0) {
					amount?.textColor = gray
					currency?.textColor = gray
				}
			}

			when (_moneyAmount) {
				0 -> delete?.visibility = View.INVISIBLE
				else -> delete?.visibility = View.VISIBLE
			}
		}
	}

	fun bottomSheetPinAppend(v: View) {
		val btnVal = v.tag.toString()

		if (_enteredPin.length >= 4) return

		_enteredPin = "$_enteredPin$btnVal"

		updatePinPlaceholder()
	}

	fun bottomSheetPinDelete(v: View) {

		if (_enteredPin.isEmpty()) return

		_enteredPin = _enteredPin.substring(0, _enteredPin.length - 1)

		updatePinPlaceholder()
	}

	fun bottomSheetPhoneNumSubmit(v: View) {
		val num = _phoneNumber

		if (num.length == 8) {
			val formatted = num.replace(
				"""([0-9])([0-9]{3})([0-9]{4})""".toRegex(), {
				val g = it.groupValues

				"09${g[1]} ${g[2]} ${g[3]}"
			})
			val contact = Contact("", formatted)
			_airtimeContact = contact
			bottomSheetAmountShow()
		}
	}

	fun bottomSheetSummarySubmit(v: View) {

		when (_bottomSheetState) {
			"buy_airtime" -> {
				if (_airtimeSubject == 1) {
					cbeUssdCall(3, _airtimeSubject, _moneyAmount, _enteredPin, 1)
				} else {
					_airtimeContact?.number?.replace(" ", "")?.let {
						cbeUssdCall(3, 2, 1, it, _moneyAmount, _enteredPin, 1)
					}
				}
			}

			"send_money" -> {
				_airtimeContact?.number?.replace(" ", "")?.let {
					cbeUssdCall(1, it, _moneyAmount, _enteredPin, 1)
				}
			}

			"make_payment" -> {
				cbeUssdCall(4, _tillNumber, _moneyAmount, _enteredPin, 1)
			}

			"withdraw_money" -> {
				cbeUssdCall(2, 1, _tillNumber, _moneyAmount, _enteredPin, 1)
			}
		}

		_bsDialog.hide()
	}

	private fun bottomSheetAmountShow(amount: Int = 0) {
		setBottomSheetLayout(R.layout.bottom_sheet_amount)

		val moneyAmount = _bsDialog.findOptional<TextView>(R.id.bottomSheetAmountText)

		moneyAmount?.text = if (amount > 0) {
			amount.toString()
		} else {
			_moneyAmount.toString()
		}

		bottomSheetAmountUpdate()

		val amountTitle = _bsDialog.findOptional<TextView>(R.id.bottomSheetAmountTitle)

		when (_bottomSheetState) {
			"buy_airtime" -> {
				amountTitle?.text = getString((R.string.airtime_amount))
			}

			"send_money" -> {
				amountTitle?.text = getString(R.string.send_money_amount_title)
			}

			"make_payment" -> {
				amountTitle?.text = getString(R.string.make_payment_amount_title)
			}

			"withdraw_money" -> {
				amountTitle?.text = getString(R.string.withdraw_money_amount_title)
			}

			else -> {
				amountTitle?.text = getString(R.string.money_amount_title)
			}
		}
	}

	fun bottomSheetPinBack(v: View) {

		when (_bottomSheetState) {
			"setting" -> bottomSheetSettingsShow(v)
			else -> bottomSheetAmountShow()
		}
	}

	fun bottomSheetSummaryBack(v: View) {
		when (_bottomSheetState) {
			"buy_airtime" -> {
				_airtimeSubject = 1
				bottomSheetAmountShow()
			}

			"send_money" -> {
				bottomSheetAmountShow()
			}

			"make_payment" -> {
				bottomSheetAmountShow()
			}

			"withdraw_money" -> {
				bottomSheetAmountShow()
			}

			else -> {
			}
		}
	}

	fun bottomSheetContactsBack(v: View) {
		when (_bottomSheetState) {
			"buy_airtime" -> {
				_airtimeSubject = 1
				setBottomSheetLayout(R.layout.bottom_sheet_who)
			}

			"send_money" -> {
				bottomSheetAmountShow()
			}

			else -> {
			}
		}
	}

	fun bottomSheetPhoneNum(v: View) {
		setBottomSheetLayout(R.layout.bottom_sheet_phone_number)

		_phoneNumber = ""
		bottomSheetPhoneNumUpdate()
	}

	fun bottomSheetPhoneNumAppend(v: View) {
		val btnVal = v.tag.toString()

		if (_phoneNumber.length in 0..7) {
			_phoneNumber = "$_phoneNumber$btnVal"

			bottomSheetPhoneNumUpdate()
		}
	}

	fun bottomSheetPhoneNumDelete(v: View) {
		if (_phoneNumber.isNotEmpty()) {
			_phoneNumber = _phoneNumber.substring(0, _phoneNumber.length - 1)

			bottomSheetPhoneNumUpdate()
		}
	}

	private fun bottomSheetPhoneNumUpdate() {
		val bs = _bsDialog

		val numBlocks = listOf(
			R.id.phoneNum1,
			R.id.phoneNum2,
			R.id.phoneNum3,
			R.id.phoneNum4,
			R.id.phoneNum5,
			R.id.phoneNum6,
			R.id.phoneNum7,
			R.id.phoneNum8
		)

		numBlocks.forEach {
			val block = bs.findOptional<TextView>(it)
			block?.text = "0"
			block?.textColor = ContextCompat.getColor(this, R.color.gray200)
		}

		_phoneNumber.forEachIndexed { i, char ->
			val block = bs.findOptional<TextView>(numBlocks[i])
			block?.text = char.toString()
			block?.textColor = ContextCompat.getColor(this, R.color.text400)
		}

		val deleteBtn = bs.findOptional<ImageView>(R.id.bottomSheetPhoneNumDelete)
		if (_phoneNumber.isEmpty()) {
			deleteBtn?.visibility = View.INVISIBLE
		} else {
			deleteBtn?.visibility = View.VISIBLE
		}

		val submitBtn = bs.findOptional<ImageView>(R.id.bottomSheetPhoneNumSubmit)
		if (_phoneNumber.length == 8) {
			submitBtn?.setImageResource(R.drawable.ic_ok_green)
		} else {
			submitBtn?.setImageResource(R.drawable.ic_ok_gray)
		}
	}

	fun bottomSheetAmountBack(v: View) {
		when (_bottomSheetState) {
			"buy_airtime" -> {
				setBottomSheetLayout(R.layout.bottom_sheet_who)
			}

			"send_money" -> {
				bottomSheetContactsShow()
			}

			"make_payment" -> {
				bottomSheetTillShow()
			}

			"withdraw_money" -> {
				bottomSheetTillShow()
			}

			else -> {
			}
		}
	}

	private fun bottomSheetContactsShow() {
		withPermission(READ_CONTACTS) {
			setBottomSheetLayout(R.layout.bottom_sheet_contacts)
			populateBottomSheetContacts()

			val title = _bsDialog.findOptional<TextView>(R.id.bottomSheetContactsTitle)
			title?.text = getText(R.string.select_contact)

			if (_bottomSheetState == "send_money") {
				val backBtn = _bsDialog.findOptional<ImageView>(R.id.bottomSheetContactsBack)

				backBtn?.visibility = View.INVISIBLE
			}
		}
	}

	private fun bottomSheetTillShow() {
		_tillNumber = ""

		val tillLayout = R.layout.bottom_sheet_till
		setBottomSheetLayout(tillLayout)
		updateTillNumberState()

		val tillTitle = _bsDialog.findOptional<TextView>(R.id.bottomSheetTillTitle)

		when (_bottomSheetState) {
			"make_payment" -> tillTitle?.text = getText(R.string.make_payment_till_title)
			"withdraw_money" -> tillTitle?.text = getText(R.string.withdraw_money_till_title)
		}
	}

	fun bottomSheetTillSubmit(v: View) {
		if (_tillNumber.length != 5) return

		bottomSheetAmountShow()

		val amount = _bsDialog.findOptional<TextView>(R.id.bottomSheetAmountText)
		amount?.text = "0"

		val title = _bsDialog.findOptional<TextView>(R.id.bottomSheetAmountTitle)

		when (_bottomSheetState) {
			"make_payment" -> title?.text = getText(R.string.make_payment_amount_title)
			"withdraw_money" -> title?.text = getText(R.string.withdraw_money_amount_title)
		}
	}

	fun bottomSheetContactSelect(v: View) {
		v.tag?.let {
			val contact = it as Contact
			_airtimeContact = contact

			when (_bottomSheetState) {
				"buy_airtime" -> bottomSheetAmountShow()
				"send_money" -> bottomSheetAmountShow()
			}
		}
	}

	fun bottomSheetPinSubmit(v: View) {
		when (_bottomSheetState) {
			"setting" -> {
				when (_settingState) {
					"old_pin" -> {
						if (_enteredPin.length == 4 && verifyPin()) {
							_settingState = "new_pin"
							_oldPin = _enteredPin
							bottomSheetPinShow()
						}
					}
					"new_pin" -> {
						if (_enteredPin.length == 4) {
							_settingState = "confirm_pin"
							_newPin = _enteredPin
							bottomSheetPinShow()
						}
					}
					"confirm_pin" -> {
						if (_enteredPin.length == 4) {
							if (_enteredPin == _newPin) {
								setPref("pin_code", _newPin)

								_pinRedirect.let {
									if (it != null) {
										_bottomSheetState = it
										bottomSheetSummaryShow()
										_pinRedirect = null
									} else {
										_bsDialog.hide()

										cbeUssdCall(7, 2, _oldPin, _newPin, _newPin)
									}
								}
							} else {
								vibrate()
								toast(R.string.pins_dont_match, type = "error")

								bottomSheetPinShow()
							}
						}
					}
				}
			}
			else -> {
				if (!isMoneyAmountValid()) {
					return
				}
				if (verifyPin()) bottomSheetSummaryShow()
			}
		}
	}

	fun bottomSheetAbout(v: View) {
		setBottomSheetLayout(R.layout.bottom_sheet_about)

		_bsDialog.let {
			val version = BuildConfig.VERSION_NAME
			val versionView = it.findOptional<TextView>(R.id.aboutVersion)
			versionView?.text = String.format(getString(R.string.version), version)
		}
	}

	private fun bottomSheetSummaryShow() {
		val layout = R.layout.bottom_sheet_summary
		val bottomSheetView = layoutInflater.inflate(layout, activityMain, false)

		bottomSheetView.findOptional<TextView>(R.id.summary_amount)?.text = _moneyAmount.toString()

		val title = bottomSheetView.findOptional<TextView>(R.id.textSummaryTitle)
		val contactName = bottomSheetView.findOptional<TextView>(R.id.textSummaryContactName)
		val contactNumber = bottomSheetView.findOptional<TextView>(R.id.textSummaryContactNumber)
		val submitText = bottomSheetView.findOptional<TextView>(R.id.summarySubmitText)
		val contact = _airtimeContact

		when (_bottomSheetState) {
			"buy_airtime" -> {
				title?.text = getString(R.string.buying_airtime)
				submitText?.text = getString(R.string.buy)

				if (_airtimeSubject == 2) {

					if (contact !== null) {
						contactName?.text = String.format(getString(R.string.for_contact), contact.name)
						contactNumber?.visibility = View.VISIBLE
						contactNumber?.text = contact.number
					}
				} else {
					contactName?.text = getString(R.string.for_your_number)
					contactNumber?.visibility = View.GONE
				}
			}

			"send_money" -> {
				title?.text = getString(R.string.send_money)
				submitText?.text = getString(R.string.send)

				if (contact !== null) {
					contactName?.text = String.format(getString(R.string.to_contact), contact.name)
					contactNumber?.text = contact.number
				}
			}

			"make_payment" -> {
				title?.text = getString(R.string.make_payment)
				submitText?.text = getString(R.string.pay)

				contactName?.text = getString(R.string.merchant_code)
				contactNumber?.text = _tillNumber.replace("""([0-9])""".toRegex(), {
					"${it.groupValues[1]} "
				})
			}

			"withdraw_money" -> {
				title?.text = getString(R.string.withdraw_money)
				submitText?.text = getString(R.string.withdraw)

				contactName?.text = getString(R.string.agent_code)
				contactNumber?.text = _tillNumber.replace("""([0-9])""".toRegex(), {
					"${it.groupValues[1]} "
				})
			}
			else -> {
			}
		}

		_bsDialog.setContentView(bottomSheetView)
		_bsDialog.show()
	}

	fun bottomSheetAmountSubmit(v: View) {

		if (!isMoneyAmountValid()) return
		bottomSheetPinShow()
	}

	fun bottomSheetSettingPin(v: View) {
		bottomSheetSettingPin()
	}

	private fun bottomSheetSettingPin() {
		_bottomSheetState = "setting"
		getPrefString("pin_code").let {
			_settingState = if (it != "") "old_pin"
			else "new_pin"
		}

		bottomSheetPinShow()
	}

	private fun bottomSheetPinShow() {
		_enteredPin = ""

		setBottomSheetLayout(R.layout.bottom_sheet_pin)

		val bs = _bsDialog
		val pinTitle = bs.findOptional<TextView>(R.id.bottomSheetPinTitle)
		val pinBack = bs.findOptional<ImageView>(R.id.bottomSheetAmountBack)

		when (_bottomSheetState) {
			"setting" -> {
				when (_settingState) {
					"old_pin" -> {
						pinTitle?.text = getString(R.string.enter_your_current_pin)
						pinBack?.visibility = View.INVISIBLE
					}
					"new_pin" -> {
						pinTitle?.text = getString(R.string.a_new_pin)
						pinBack?.visibility = View.VISIBLE
					}
					"confirm_pin" -> {
						pinTitle?.text = getString(R.string.confirm_pin)
						pinBack?.visibility = View.VISIBLE
					}
					else -> {
						pinTitle?.text = getString(R.string.enter_your_pin)
						pinBack?.visibility = View.INVISIBLE
					}
				}
			}
			else -> {
				getPrefString("pin_code").let {
					if (it != "") {
						pinTitle?.text = getString(R.string.enter_your_pin)
					} else {
						_pinRedirect = _bottomSheetState
						_bottomSheetState = "setting"
						_settingState = "new_pin"
						pinTitle?.text = getString(R.string.enter_cbe_pin)
					}
				}
			}
		}
	}

	fun selectAirtimeSubject(v: View) {
		val subject = v.tag.toString()

		when (subject) {
			"1" -> {
				_airtimeSubject = 1
				bottomSheetAmountShow()
			}
			"2" -> {
				_airtimeSubject = 2
				bottomSheetContactsShow()
			}
		}
	}
//
//	private fun populateBottomSheetContacts() {
//		_bsDialog.apply {
//			bsContactsLoader?.show()
//
//			doAsync {
//				val contactsList = getContacts()
//
//				uiThread {
//					bsContactsList.apply {
//						adapter = ContactsListAdapter(
//							this@MainActivity,
//							contactsList.distinct()
//						)
//					}
//
//					bsContactsLoader.gone()
//				}
//			}
//		}
//	}

	private fun populateBottomSheetContacts() {
		_bsDialog.apply {
			bsContactsLoader?.show()

			doAsync {
				val contactsList = getContacts()

				uiThread {
					bsContactsLoader.gone()

					bsContactsList.apply {
						adapter = ContactsRecyclerAdapter(
							this@MainActivity,
							contactsList.distinct()
						)

						layoutManager = LinearLayoutManager(this@MainActivity)
					}
				}
			}
		}
	}

	fun appendBottomSheetTillInput(v: View) {
		val btnVal = v.tag.toString()

		if (_tillNumber.length >= 5) return

		_tillNumber = "$_tillNumber$btnVal"
		updateTillNumberState()
	}

	fun bottomSheetDeleteMakePaymentTill(v: View) {
		if (_tillNumber == "") return

		_tillNumber = _tillNumber.substring(0, _tillNumber.length - 1)
		updateTillNumberState()
	}

	private fun updateTillNumberState() {
		val bs = _bsDialog

		val blocks = listOf(
			R.id.numBlock1,
			R.id.numBlock2,
			R.id.numBlock3,
			R.id.numBlock4,
			R.id.numBlock5
		)

		blocks.map {
			bs.findOptional<TextView>(it)?.text = ""
		}

		_tillNumber.mapIndexed { i, char ->
			bs.findOptional<TextView>(blocks[i])?.text = char.toString()
		}

		val submit = bs.findOptional<ImageView>(R.id.bottomSheetTillSubmit)
		val delete = bs.findOptional<ImageView>(R.id.bottomSheetNumberInputDelete)

		when (_tillNumber.length) {
			0 -> delete?.visibility = View.INVISIBLE
			in 1..5 -> delete?.visibility = View.VISIBLE
		}

		when (_tillNumber.length) {
			5 -> submit?.imageResource = R.drawable.ic_ok_green
			else -> submit?.imageResource = R.drawable.ic_ok_gray
		}
	}

	private fun isMoneyAmountValid(): Boolean {
		return when (_bottomSheetState) {
			"buy_airtime" -> _moneyAmount >= 5
			"make_payment" -> _moneyAmount >= 5
			"send_money" -> _moneyAmount >= 50
			"withdraw_money" -> _moneyAmount >= 50
			else -> _moneyAmount >= 5
		}
	}

	private fun updatePinPlaceholder() {
		_bsDialog.let { bs ->
			val pinBullets = listOf(
				R.id.pinBullet1,
				R.id.pinBullet2,
				R.id.pinBullet3,
				R.id.pinBullet4
			)

			val submit = bs.findOptional<ImageView>(R.id.bottomSheetPinSubmit)
			val delete = bs.findOptional<ImageView>(R.id.bottomSheetPinDelete)
			val length = _enteredPin.length

			pinBullets.forEachIndexed { i, it ->
				val pinBullet = bs.findOptional<ImageView>(it)

				pinBullet?.imageResource = if (i < length) {
					R.drawable.ic_pin_bullet_dark
				} else {
					R.drawable.ic_pin_bullet_light
				}
			}

			when (length) {
				4 -> submit?.imageResource = R.drawable.ic_ok_green
				else -> submit?.imageResource = R.drawable.ic_ok_gray
			}

			when (length) {
				0 -> delete?.visibility = View.INVISIBLE
				else -> delete?.visibility = View.VISIBLE
			}
		}
	}

	fun showTransactionActivity(v: View) {
		startActivity<TransactionActivity>()
	}

	fun scanQr(v: View) {
		withPermission(CAMERA) {
			val intent = Intent(applicationContext, BarcodeCaptureActivity::class.java)
			startActivityForResult(intent, BARCODE_READER_REQUEST_CODE)
		}
	}

	fun parseQrResult(result: String) {
		val qrRegex = """https?://(?:www\.)?birr.io/qr\?(cbemerch|cbeagent)=([0-9]{5})?(.*)""".toRegex()
		val tillRegex = """&?(cbemerch|cbeagent)=([0-9]{5})""".toRegex()
		val amtRegex = """&amt=([0-9]+)""".toRegex()
		val nameRegex = """&name=([a-zA-Z0-9\s\-]+)""".toRegex()
		val resetRegex = """http://birr.io/qr?BRIO_RESET_251_90_418_3553""".toRegex()

		if (result.matches(qrRegex)) {

			tillRegex.find(result)?.let {
				_bottomSheetState = when (it.groupValues[1]) {
					"cbeagent" -> "withdraw_money"
					else -> "make_payment"
				}
				_tillNumber = it.groupValues[2]
			}

			amtRegex.find(result)?.let {
				_moneyAmount = it.groupValues[1].let {
					if (it.isEmpty()) "0" else it
				}.toInt()
			}

			nameRegex.find(result)?.let { toast(it.groupValues[1]) }

			ToneGenerator(AudioManager.STREAM_MUSIC, 100)
				.startTone(ToneGenerator.TONE_PROP_ACK, 500)

			bottomSheetAmountShow(_moneyAmount)
			updateTillNumberState()
		} else {
			vibrate()
			toast(R.string.invalid_qr, Toast.LENGTH_LONG, "error")
		}

		if (result.matches(resetRegex)) {
			toast("reset")
			resetBrio()
		}
	}

	private fun resetBrio() {
		resetData()
	}

	fun signOut(v: View) {
		resetData()
		startActivity<StartActivity>()
	}

	fun setLanguage(v: View) {
		val lang = v.tag

		setPref("locale", lang)

		bottomSheetRefreshLang()
		refreshLocale()
		recreate()
	}

	fun selectSim(v: View) {
		val sim = v.tag.toString().toInt()

		setPref("selected_sim", sim)
		bottomSheetRefreshSim()
	}

	fun setGender(v: View) {
		val gender = v.tag.toString()

		setPref("profile_gender", gender)
		bottomSheetRefreshGender()
	}

	private fun bottomSheetRefreshGender() {
		getPrefString("profile_gender").let { gender ->
			_bsDialog.let { bs ->
				val maleCheckmark = bs.findOptional<ImageView>(R.id.genderMaleCheckmark)
				val femaleCheckmark = bs.findOptional<ImageView>(R.id.genderFemaleCheckmark)

				maleCheckmark?.imageResource = R.drawable.ic_ok_gray
				femaleCheckmark?.imageResource = R.drawable.ic_ok_gray

				when (gender) {
					GENDER_MALE -> maleCheckmark?.imageResource = R.drawable.ic_ok_green
					GENDER_FEMALE -> femaleCheckmark?.imageResource = R.drawable.ic_ok_green
				}
			}
		}
	}

	private fun bottomSheetRefreshLang() {
		getPrefString("locale").let { lang ->
			_bsDialog.let {
				val amCheckmark = it.findOptional<ImageView>(R.id.langAmharicCheckmark)
				val enCheckmark = it.findOptional<ImageView>(R.id.langEnglishCheckmark)

				amCheckmark?.imageResource = R.drawable.ic_ok_gray
				enCheckmark?.imageResource = R.drawable.ic_ok_gray

				AppData.apply {
					when (lang) {
						LOCALE_AM -> amCheckmark?.imageResource = R.drawable.ic_ok_green
						LOCALE_EN -> enCheckmark?.imageResource = R.drawable.ic_ok_green
						else -> setPref("locale", LOCALE_AM)
					}
				}
			}
		}
	}

	private fun bottomSheetRefreshSim() {
		_bsDialog.let { bs ->
			val simOption = bs.findOptional<LinearLayout>(R.id.simOption)

			withPermission(READ_PHONE_STATE) {
				if (Build.VERSION.SDK_INT >= 22) {
					if (getSimList()?.size == 1) {
						simOption?.visibility = View.GONE
					} else {
						getPrefInt("selected_sim").let { sim ->
							val sim1Icon = bs.findOptional<ImageView>(R.id.sim1Icon)
							val sim2Icon = bs.findOptional<ImageView>(R.id.sim2Icon)

							sim1Icon?.imageResource = R.drawable.ic_sim_card
							sim2Icon?.imageResource = R.drawable.ic_sim_card

							when (sim) {
								0 -> sim1Icon?.imageResource = R.drawable.ic_sim_card_green
								1 -> sim2Icon?.imageResource = R.drawable.ic_sim_card_green
								else -> setPref("selected_sim", 0)
							}
						}

						simOption?.visibility = View.VISIBLE
					}
				} else {
					simOption?.visibility = View.GONE
				}
			}
		}
	}

	private fun verifyPin(): Boolean {
		return getPrefString("pin_code").let {

			if (_enteredPin.length == 4 && _enteredPin != it) {
				vibrate()
				toast(R.string.incorrect_pin, type = "error")

				_enteredPin = ""
				updatePinPlaceholder()
			}

			_enteredPin == it
		}
	}

	private fun bottomSheetAirtimeReceiverShow() {
		setBottomSheetLayout(R.layout.bottom_sheet_who)
	}

	fun bottomSheetSettingsShow(v: View) {
		withPermission(READ_PHONE_STATE) {
			val telMan = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

			try {
				val subId = telMan.subscriberId
				val simSerial = telMan.simSerialNumber
			} catch (e: SecurityException) {
				Log.e("MainActivity", e.message)
			}
		}
		setBottomSheetLayout(R.layout.bottom_sheet_settings)
//		bottomSheetSettingsToggleActivation()
		bottomSheetRefreshSim()
		bottomSheetRefreshLang()
		refreshLocale()
	}

	fun bottomSheetProfileShow(v: View) {
		setBottomSheetLayout(R.layout.bottom_sheet_profile)

//		checkPhoneNumVerification()

		_bsDialog.apply {

			val nameMap = mapOf(
				"first" to getPrefString("first_name"),
				"middle" to getPrefString("middle_name"),
				"last" to getPrefString("last_name")
			)

			val birthdateMap = mapOf(
				"day" to getPrefString("birth_day"),
				"month" to getPrefString("birth_month"),
				"year" to getPrefString("birth_year")
			)

			val name = "${nameMap["first"]} ${nameMap["middle"]} ${nameMap["last"]}"

			val dateFormat = SimpleDateFormat("dd MM yyyy")
			val birthdate = dateFormat.parse("${birthdateMap["day"]} ${birthdateMap["month"]} ${birthdateMap["year"]}")

			val birthdateText = "${getString(R.string.birthdate)}: " + if (
				resources.configuration.locale.toString() in listOf(
					LOCALE_AM,
					"${LOCALE_AM}_$REGION_MASCULINE",
					"${LOCALE_AM}_$REGION_FEMININE"
				)
			) {
				val date = birthdate.toAmharicDate()
				val month = date["month"]?.toAmharicMonthName()
				val day = date["day"]
				val year = date["year"]
				"$month $day $year"
			} else {
				SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH).format(birthdate)
			}

			profileName.text = name
			profileBirthdate.text = birthdateText

			getPrefString("phone_num").let {
				profilePhoneNum.apply {
					if (it == "") {
						visibility = View.GONE
					} else {
						visibility = View.VISIBLE
						text = String.format(getString(R.string.phone_number), it.formatPhoneNumber())
					}
				}
			}

			bottomSheetRefreshGender()
		}
	}

	private fun sendActivationText() {
		withPermission(Manifest.permission.SEND_SMS) {
			if (!isAirplaneModeOn()) {
				val version = BuildConfig.VERSION_NAME
				sendSMS(
					"+251904183553",
					"""BRIO_VERIFICATION $version
						|id: ${FirebaseInstanceId.getInstance().id}
					""".trimMargin()
				)
				setPref("is_phone_num_verified", true)

				// save brio's contact so that people wont freak out when they see the verification sms
				withPermission(WRITE_CONTACTS) {
					saveBrioContact()
				}

			} else {
				toast("Please turn off Airplane Mode")
//				checkPhoneNumVerification()
			}
		}
	}

	private fun checkCbeActivation() {

		doAsync {

			if (getPrefBoolean("is_cbe_activated")) {
				val activatedSms = _cbeSmsList.filter {
					val body = it.toMap()["body"] ?: ""

					body.matches(ResponsePatterns.BUY_AIRTIME.toRegex()) ||
						body.matches(ResponsePatterns.SEND_MONEY.toRegex()) ||
						body.matches(ResponsePatterns.BALANCE_CREDIT.toRegex()) ||
						body.matches(ResponsePatterns.BALANCE_WITHDRAWAL.toRegex()) ||
						body.matches(ResponsePatterns.MAKE_PAYMENT.toRegex())
				}

				uiThread {
					if (activatedSms.isNotEmpty()) {
						setPref("is_cbe_activated", true)
					} else {
						setPref("is_cbe_activated", false)

						alert {

							this.customView = layoutInflater.inflate(
								R.layout.waiting_confirmation, null
							)
						}.show()
					}
				}
			}
		}
	}

	private fun checkPhoneNumVerification() {
		val verificationRegex = """BRIO_VERIFIED[\s]+id: ([a-zA-Z0-9\-_]+)[\s]+phone_num: ([0-9]{10})""".toRegex()

		if (!getPrefBoolean("is_phone_num_verified")) {
			getBrioSmsList(this).let {
				it.filter {
					val body = it.toMap()["body"] ?: ""

					body.matches(verificationRegex)
				}.let {
					if (it.isNotEmpty()) {
						it.forEach {
							val body = it.toMap()["body"] ?: ""

							verificationRegex.find(body)?.let {
								if (it.groupValues[1] == FirebaseInstanceId.getInstance().id) {
									setPref("phone_num", it.groupValues[2])
									setPref("is_phone_num_verified", true)
								}
							}
						}
					}
				}
			}

			// check again
			if (!getPrefBoolean("is_phone_num_verified")) {
				alert {
					isCancelable = false
					message = getString(R.string.number_not_verified_message)

					okButton {
						sendActivationText()
						setPref("is_phone_num_verified", true)
					}

				}.show()
			}
		}
	}

	private fun setBottomSheetLayout(layout: Int, expanded: Boolean = true) {
		_bsDialog.let { bs ->
			layoutInflater.inflate(layout, null).let {
				bs.setContentView(it)
				bs.show()

				BottomSheetBehavior.from(it.parent as View).let {
					if (expanded) it.state = STATE_EXPANDED
				}
			}
		}
	}

	companion object {
		private val TAG = MainActivity::class.java.name

		private const val BARCODE_READER_REQUEST_CODE = 1
	}
}