package io.birr.brio_main.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import io.birr.brio_main.R
import io.birr.brio_main.utils.Contact
import kotlinx.android.synthetic.main.list_item_contact.view.*


class ContactsRecyclerAdapter(val context: Context, private val contacts: List<Contact>) : RecyclerView.Adapter<ContactsRecyclerAdapter.ViewHolder>() {

	inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
		val name: TextView? = view.contactName
		val number: TextView? = view.contactNumber
		val item: LinearLayout? = view.contactItem
	}

	override fun onBindViewHolder(viewHolder: ContactsRecyclerAdapter.ViewHolder, i: Int) {
		val contact = contacts[i]

		viewHolder.apply {
			name?.text = contact.name
			number?.text = contact.number
			item?.tag = contact
		}
	}

	override fun getItemCount() = contacts.size

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsRecyclerAdapter.ViewHolder {
		val inflater = LayoutInflater.from(parent.context)

		val view = inflater.inflate(R.layout.list_item_contact, parent, false)

		return ViewHolder(view)
	}
}
