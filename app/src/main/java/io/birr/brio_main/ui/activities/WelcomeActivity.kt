package io.birr.brio_main.ui.activities

import android.Manifest.permission.*
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import io.birr.brio_main.R
import io.birr.brio_main.model.getPrefBoolean
import io.birr.brio_main.model.setPref
import io.birr.brio_main.ui.adapters.TourPagerAdapter
import io.birr.brio_main.ui.fragments.*
import io.birr.brio_main.utils.*
import io.birr.brio_main.utils.ResponsePatterns.ACCOUNT_INIT
import io.birr.brio_main.utils.SmsHandler.getCbeSmsList
import kotlinx.android.synthetic.main.activity_start_pin.*
import kotlinx.android.synthetic.main.activity_welcome.*
import kotlinx.android.synthetic.main.fragment_sign_up_birthdate.*
import kotlinx.android.synthetic.main.fragment_sign_up_names.*
import org.jetbrains.anko.findOptional
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.startActivity
import java.lang.Math.abs

class WelcomeActivity : AppCompatActivity() {

	private var _signUpFirstName: String? = null
	private var _signUpMiddleName: String? = null
	private var _signUpLastName: String? = null
	private var _signUpBirthDay: String? = null
	private var _signUpBirthMonth: String? = null
	private var _signUpBirthYear: String? = null

	// TODO replace Runnables with RxJava
	private var _cbeSmsRunnable: Runnable? = null
	private var _pinCodeRunnable: Runnable? = null
	private var _cbeSmsList: List<List<Pair<String, String>>> = listOf()
	private var _isCbeRegistered: Boolean = false
	private var _defaultPin = ""
	private var _enteredPin = ""
	private val _layouts = arrayOf(R.layout.tour_slide_1, R.layout.tour_slide_2, R.layout.tour_slide_3, R.layout.tour_slide_4, R.layout.tour_slide_5)

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_welcome)
		refreshLocale()

		_isCbeRegistered = getPrefBoolean("is_cbe_registered")

		if (getPrefBoolean("is_tour_done")) {
			startUsing()
			return
		}

		val tourAdapter = TourPagerAdapter(this, _layouts)
		tourPager.adapter = tourAdapter

		tourPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
			override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
				val sliderPager = findOptional<LinearLayout>(R.id.sliderPager)

				sliderPager?.alpha = if (
					position == _layouts.size - 2
					&& positionOffset > 0
				) {
					(.3f - positionOffset) * 1f
				} else 1f

				val dots = listOf(R.id.sliderDot1, R.id.sliderDot2, R.id.sliderDot3, R.id.sliderDot4)

				dots.forEachIndexed { i, it ->
					findOptional<ImageView>(it)?.let { dot ->
						if (positionOffset < 0) {
							when (position) {
								i -> dot.alpha = 1f - abs(positionOffset) + .3f
								i + 1 -> dot.alpha = .3f + abs(positionOffset)
								else -> dot.alpha = .3f
							}
						} else {
							dot.alpha = when (position) {
								i -> 1f - positionOffset + .3f
								i - 1 -> .3f + positionOffset
								else -> .3f
							}
						}
					}
				}
			}

			override fun onPageSelected(position: Int) {

				val sliderPager = findOptional<LinearLayout>(R.id.sliderPager)

//				val sliderDotsLayout = findOptional<LinearLayout>(R.id.sliderDotsLayout)
//				 TODO add these dynamically

				val dots = listOf(R.id.sliderDot1, R.id.sliderDot2, R.id.sliderDot3, R.id.sliderDot4)

				dots.forEachIndexed { i, it ->
					val dot = findOptional<ImageView>(it)
					dot?.alpha = if (i == position) 1f else 0.3f
				}

				sliderPager?.visibility = when (position) {
					_layouts.size - 1 -> View.GONE
					else -> View.VISIBLE
				}
			}

			override fun onPageScrollStateChanged(state: Int) {
			}
		})
	}

	private fun updateCbeSmsList() {
		withPermission(READ_SMS) {
			_cbeSmsList = getCbeSmsList(this)
		}
	}

//	fun askAgain(v: View) {
//		withPermission(READ_SMS) {
//			withPermission(CALL_PHONE) {
//				if (_isCbeRegistered) {
//
//				}
//				cbeUssdCall(6, "0000", 1)
//			}
//		}
//	}

	fun selectSim(v: View) {
		setPref("selected_sim", v.tag.toString().toInt())

		executeRegistration()
	}

	fun submitSignUpNames(v: View? = null) {
		val firstName = firstNameInput.text.toString()
		val middleName = middleNameInput.text.toString()
		val lastName = lastNameInput.text.toString()

		when {
			firstName.isEmpty() -> toast(R.string.first_name_empty)
			middleName.isEmpty() -> toast(R.string.middle_name_empty)
			lastName.isEmpty() -> toast(R.string.last_name_empty)

			else -> {
				_signUpFirstName = firstName
				_signUpMiddleName = middleName
				_signUpLastName = lastName

				setPref("first_name", firstName)
				setPref("middle_name", middleName)
				setPref("last_name", lastName)

				setContentView(R.layout.activity_start)

				fragmentManager.beginTransaction().apply {
					replace(R.id.startActivityLayout, SignUpBirthdateFragment(), "sign_up_birthdate_frag")
					addToBackStack("sign_up_birthdate_frag")
				}.commit()
			}
		}
	}

	fun submitSignUpBirthdate(v: View? = null) {
		_signUpBirthDay = String.format(
			"%02d",
			spinnerDays.selectedItem
		)
		_signUpBirthMonth = String.format(
			"%02d",
			(spinnerMonths.selectedItemPosition + 1)
		)
		_signUpBirthYear = spinnerYears.selectedItem.toString()

		setPref("birth_day", _signUpBirthDay.orEmpty())
		setPref("birth_month", _signUpBirthMonth.orEmpty())
		setPref("birth_year", _signUpBirthYear.orEmpty())

		goToGenderFrag()
	}

	fun selectGender(v: View) {
		val gender = v.tag.toString()

		selectGender(gender)
	}

	private fun selectGender(gender: String) {
		setPref("profile_gender", gender)

		withPermission(READ_PHONE_STATE) {
			getSimList().let {
				when {
					it == null -> executeRegistration()
					it.size > 1 -> goToSimFrag()
					else -> executeRegistration()
				}

			}
		}
	}


	fun retryRegistration(v: View) {
		executeRegistration()
	}

	private fun executeRegistration() {
		if (_isCbeRegistered) {
			goToPinFrag()
		} else {
//			cbeUssdCall(
//				1,
//				_signUpFirstName.orEmpty(),
//				_signUpMiddleName.orEmpty(),
//				_signUpLastName.orEmpty(),
//				"$_signUpBirthDay$_signUpBirthMonth$_signUpBirthYear",
//				1, 1, 1
//			)
//
//			goToSignUpWaitingFrag()

			startActivity<MainActivity>()
		}
	}

//	private fun startUsing() {
//		startActivity<RegistrationActivity>()
//	}

	fun startUsing(v: View? = null) {
		checkPermissions(SEND_SMS)

		setPref("is_tour_done", true)

		val handler = Handler()

		_cbeSmsRunnable = object : Runnable {
			override fun run() {

				if (!_isCbeRegistered) {
					checkCbeRegistration()

					if (_cbeSmsList.isEmpty()) {
						_isCbeRegistered = false
						handler.postDelayed(this, 5000)
					}
				}
			}
		}

		handler.postDelayed(_cbeSmsRunnable, 0)

		if (_cbeSmsList.isEmpty()) {
			_isCbeRegistered = false
			showRegistrationPage()
		}
	}

	private fun showRegistrationPage() {
		goToSignUpNamesFrag()

//		findOptional<LinearLayout>(R.id.confirmationSimsLayout)?.let { sims ->
//			if (Build.VERSION.SDK_INT >= 22) {
//				withPermission(READ_PHONE_STATE) {
//					getSimList()?.let {
//						if (it.size > 1) sims.visibility = View.VISIBLE
//						else sims.visibility = View.GONE
//					}
//				}
//			} else sims.visibility = View.GONE
//
//		}
	}

	fun checkCbeRegistration() {
		updateCbeSmsList()

		val validCbeSms = _cbeSmsList.filter {
			val body = it.toMap()["body"] ?: ""

			val implicitBalance = body.matches(ResponsePatterns.IMPLICIT_BALANCE.toRegex())
			val wrongPin = body.matches(ResponsePatterns.WRONG_PIN.toRegex())
			val initPin = body.matches(ResponsePatterns.INIT_PIN.toRegex())
			val accountActivated = body.matches(ResponsePatterns.ACCOUNT_ACTIVATED.toRegex())

			implicitBalance || wrongPin || initPin || accountActivated
		}

		if (!validCbeSms.isEmpty()) {
			_isCbeRegistered = true
//			setCbeRegistered(true)
			setPref("is_cbe_registered", true)
		}
	}

	private fun goToPinFrag() {
		setContentView(R.layout.activity_start)

		fragmentManager.beginTransaction().apply {
			replace(R.id.startActivityLayout, PinCodeFragment(), "pin_code_frag")
			addToBackStack("pin_code_frag")
		}.commitAllowingStateLoss()
	}

	private fun goToNewPinFrag() {
		setContentView(R.layout.activity_start)

		fragmentManager.beginTransaction().apply {
			replace(R.id.startActivityLayout, NewPinCodeFragment(), "new_pin_code_frag")
			addToBackStack("new_pin_code_frag")
		}.commitAllowingStateLoss()
	}

	private fun goToGenderFrag() {
		setContentView(R.layout.activity_start)

		fragmentManager.beginTransaction().apply {
			replace(R.id.startActivityLayout, GenderFragment(), "gender_frag")
			addToBackStack("gender_frag")
		}.commitAllowingStateLoss()
	}

	private fun goToSimFrag() {
		setContentView(R.layout.activity_start)

		fragmentManager.beginTransaction().apply {
			replace(R.id.startActivityLayout, SelectSimFragment(), "select_sim_frag")
			addToBackStack("select_sim_frag")
		}.commitAllowingStateLoss()
	}

	private fun goToSignUpNamesFrag() {
		setContentView(R.layout.activity_start)
		fragmentManager.beginTransaction().apply {
			replace(R.id.startActivityLayout, SignUpNamesFragment(), "sign_up_names_frag")
			addToBackStack("sign_up_names_frag")
		}.commitAllowingStateLoss()
	}

	private fun goToSignUpWaitingFrag() {
		setContentView(R.layout.activity_start)
		fragmentManager.beginTransaction().apply {
			replace(R.id.startActivityLayout, SignUpWaitingFrag(), "sign_up_waiting_frag")
			addToBackStack("sign_up_waiting_frag")
		}.commitAllowingStateLoss()

		val handler = Handler()

		_pinCodeRunnable = object : Runnable {
			override fun run() {
				_cbeSmsList = getCbeSmsList(this@WelcomeActivity)

				val pinSms = _cbeSmsList.filter {
					val body = it.toMap()["body"] ?: ""
					body.matches(ACCOUNT_INIT.toRegex())
				}

				if (pinSms.isEmpty()) {
					_isCbeRegistered = false
					handler.postDelayed(this, 5000)
				} else {
					pinSms[0].let {
						val body = it.toMap()["body"] ?: ""

						val pin = body.replace(ACCOUNT_INIT.toRegex(), {
							it.groupValues[1]
						})

						_defaultPin = pin
					}
//					setCbeRegistered(true)
					setPref("is_cbe_registered", true)
					goToNewPinFrag()
				}
			}
		}

		handler.postDelayed(_pinCodeRunnable, 0)

	}

	fun accountYes(v: View) {
		withPermission(READ_PHONE_STATE) {
			fragmentManager.beginTransaction().apply {
				if (Build.VERSION.SDK_INT >= 22) {
					getSimList()?.let {
						if (it.size > 1) {
							replace(R.id.startActivityLayout, SelectSimFragment(), "select_sim_frag")
							addToBackStack("select_sim_frag")
						} else {
							replace(R.id.startActivityLayout, PinCodeFragment(), "pin_code_frag")
							addToBackStack("pin_code_frag")
						}
					}
				} else {
					replace(R.id.startActivityLayout, PinCodeFragment(), "pin_code_frag")
					addToBackStack("pin_code_frag")
				}
			}.commitAllowingStateLoss()
		}
	}

	fun accountNo(v: View) {
		fragmentManager.beginTransaction().apply {
			replace(R.id.startActivityLayout, GoRegisterFragment(), "go_register_frag")
			addToBackStack("go_register_frag")
		}.commit()
	}

	fun appendPin(v: View) {
		val btnVal = v.tag.toString()

		if (_enteredPin.length >= 4) {
			return
		}

		_enteredPin = "$_enteredPin$btnVal"

		updatePinBullets()
	}

	fun deletePin(v: View) {

		if (_enteredPin.isEmpty()) return

		_enteredPin = _enteredPin.substring(0, _enteredPin.length - 1)

		updatePinBullets()
	}

	private fun updatePinBullets() {
		val darkBullet = R.drawable.ic_pin_bullet_dark
		val lightBullet = R.drawable.ic_pin_bullet_light

		val pinBullets = listOf(
			pinBullet1, pinBullet2, pinBullet3, pinBullet4
		)

		pinBullets.forEachIndexed { i, it ->
			it.imageResource = if (i < _enteredPin.length) darkBullet else lightBullet
		}

		pinSubmit.imageResource = if (_enteredPin.length == 4)
			R.drawable.ic_ok_green else R.drawable.ic_ok_gray
	}

	fun checkPin(view: View) {
		val pin = _enteredPin

		if (pin.length != 4) return

//		cbeUssdCall(0, 1, pin, 1)

		setPref("pin_code", pin)
		startActivity<MainActivity>()
	}

	fun setNewPin(view: View) {
		val pin = _enteredPin

		if (pin.length != 4) return

		setPref("pin_code", pin)

		startActivity<MainActivity>()
		cbeUssdCall(0, 1, _defaultPin, pin, pin)
	}

	fun next(v: View) {
		val current = getItem(+1)

		if (current < _layouts.size) {
			tourPager.currentItem = current
		}
	}

	private fun getItem(i: Int): Int {
		return tourPager.currentItem + i
	}
}
