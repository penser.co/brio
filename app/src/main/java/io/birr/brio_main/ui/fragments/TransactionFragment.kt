package io.birr.brio_main.ui.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.birr.brio_main.R
import io.birr.brio_main.utils.ResponsePatterns.BALANCE_CREDIT
import io.birr.brio_main.utils.ResponsePatterns.BUY_AIRTIME
import io.birr.brio_main.utils.ResponsePatterns.CHECK_BALANCE
import io.birr.brio_main.utils.ResponsePatterns.RECEIVE_MONEY

class TransactionFragment : Fragment() {

	// TODO: Rename and change types of parameters
	private var mParam1: String? = null
	private var mParam2: String? = null

	private var mListener: OnFragmentInteractionListener? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		if (arguments != null) {
			mParam1 = arguments!!.getString(ARG_PARAM1)
			mParam2 = arguments!!.getString(ARG_PARAM2)
		}
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
	                          savedInstanceState: Bundle?): View? {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_transaction, container, false)
	}

	// TODO: Rename method, update argument and hook method into UI event
	fun onButtonPressed(uri: Uri) {
		if (mListener != null) {
			mListener!!.onFragmentInteraction(uri)
		}
	}

	override fun onAttach(context: Context?) {
		super.onAttach(context)

		if (context is OnFragmentInteractionListener) {
			mListener = context
		} else {
			throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
		}

		pullSms()
	}

	override fun onDetach() {
		super.onDetach()
		mListener = null
	}

	interface OnFragmentInteractionListener {
		fun onFragmentInteraction(uri: Uri)
	}

	companion object {
		private val ARG_PARAM1 = "param1"
		private val ARG_PARAM2 = "param2"

		fun newInstance(param1: String, param2: String): TransactionFragment {
			val fragment = TransactionFragment()
			val args = Bundle()
			args.putString(ARG_PARAM1, param1)
			args.putString(ARG_PARAM2, param2)
			fragment.arguments = args
			return fragment
		}
	}

	private fun pullSms() {

		val activityContext = context ?: return
		val smsList = mutableListOf<List<Pair<String, String>>>()

		val cursor = activityContext.contentResolver?.query(Uri.parse("content://sms/inbox"), null, null, null, null)

		cursor ?: return

		if (cursor.moveToFirst()) { // must check the result to prevent exception
			do {
				val msgDict = mutableListOf<Pair<String, String>>()

				for (i in 0 until cursor.columnCount) {
					msgDict.add(Pair(cursor.getColumnName(i), cursor.getString(i)))
				}

				val address = msgDict.toList().find { it.first == "address" }?.second?.toLowerCase()
				if (address == "cbe birr") {
					smsList.add(msgDict)
				}

//				transactionListView.adapter = TransactionListAdapter(activityContext, smsList.toList().filter {
//					isTxn(it)
//				})

			} while (cursor.moveToNext())
		} else {
			// empty box, no SMS
		}

		cursor.close()
	}


	private fun isTxn(sms: List<Pair<String, String>>): Boolean {
		val body = sms.find {
			it.first == "body"
		}?.second

		body ?: return false
		return getSmsType(body).first == "TXN"
	}

	private fun getSmsType(sms: String): Pair<String, String> {

		return when {
			(sms.matches(BUY_AIRTIME.toRegex())) -> Pair("TXN", "BUY_AIRTIME")
			(sms.matches(BALANCE_CREDIT.toRegex())) -> Pair("TXN", "BALANCE_CREDIT")
			(sms.matches(RECEIVE_MONEY.toRegex())) -> Pair("INFO", "RECEIVE_MONEY")
			(sms.matches(CHECK_BALANCE.toRegex())) -> Pair("INFO", "CHECK_BALANCE")
			else -> Pair("NOT_PARSED", "NOT_PARSED")
		}
	}
}
