package io.birr.brio_main.ui.adapters

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.birr.brio_main.R
import org.jetbrains.anko.findOptional

class TourPagerAdapter(private val context: Context, private val layouts: Array<Int>) : PagerAdapter() {

	override fun getCount() = layouts.size

	override fun isViewFromObject(view: View, `object`: Any) = view == `object`

	override fun instantiateItem(container: ViewGroup, position: Int): Any {
		val view = getPagerAdapter().inflate(layouts[position], container, false)
		container.addView(view)

		val tac = container.findOptional<TextView>(R.id.termsAndConditions)
		tac?.movementMethod = LinkMovementMethod.getInstance()

		return view
	}

	override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
		val view = `object` as View
		container.removeView(view)
	}

	private fun getPagerAdapter(): LayoutInflater {
		return context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
	}

}
