package io.birr.brio_main.ui.fragments

import android.content.Context
import android.support.design.widget.BottomSheetDialog

open class RoundedBottomSheetDialog(val ctx: Context) : BottomSheetDialog(ctx) {

}