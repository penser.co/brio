package io.birr.brio_main.ui.fragments

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.birr.brio_main.R

class TourFragment : Fragment() {

	private val _layouts = arrayOf(R.layout.tour_slide_1, R.layout.tour_slide_2, R.layout.tour_slide_3)

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
	}

	override fun onAttach(context: Context?) {
		super.onAttach(context)
		// create background threads for long running tasks
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		val view = inflater.inflate(R.layout.activity_welcome, container, false)
//
//		val tac = findOptional<TextView>(R.id.termsAndConditions)
//		tac?.movementMethod = LinkMovementMethod.getInstance()

		return view
	}

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
	}
}
