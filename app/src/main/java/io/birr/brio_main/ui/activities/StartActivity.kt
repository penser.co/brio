package io.birr.brio_main.ui.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import io.birr.brio_main.R
import io.birr.brio_main.model.getPrefBoolean
import io.birr.brio_main.model.getPrefString
import io.birr.brio_main.model.setPref
import io.birr.brio_main.utils.refreshLocale
import org.jetbrains.anko.startActivity

class StartActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setContentView(R.layout.splash_screen)

		Handler().postDelayed({
			if (getPrefBoolean("is_cbe_skipped")) {
				val intent = Intent(this, MainActivity::class.java)
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
				startActivity(intent)

				setPref("is_cbe_skipped", false)
				finish()
			} else {

				getPrefString("pin_code").let {
					if (getPrefBoolean("is_tour_done")) {
						if (it.matches("""[0-9]{4}""".toRegex())) {
							val intent = Intent(this, MainActivity::class.java)
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
							startActivity(intent)
						} else {
							val intent = Intent(this, WelcomeActivity::class.java)
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
							startActivity(intent)
						}

						finish()
					} else {
						setContentView(R.layout.lang)
					}

				}
			}

		}, 1000)
	}

	override fun onDestroy() {
		super.onDestroy()
		Runtime.getRuntime().gc()
	}

	fun selectLang(v: View) {
		setPref("locale", v.tag.toString())
		refreshLocale()

		startActivity<WelcomeActivity>()
	}
}
