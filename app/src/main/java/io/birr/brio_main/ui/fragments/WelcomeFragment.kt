package io.birr.brio_main.ui.fragments

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.birr.brio_main.R
import org.jetbrains.anko.findOptional

class WelcomeFragment : Fragment() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
	}

	override fun onAttach(context: Context?) {
		super.onAttach(context)
		// create background threads for long running tasks
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.activity_start_welcome, container, false)
	}

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)

		val tac = findOptional<TextView>(R.id.termsAndConditions)
		tac?.movementMethod = LinkMovementMethod.getInstance()
	}
}
