package io.birr.brio_main.ui.fragments

import android.app.Fragment
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import io.birr.brio_main.R
import kotlinx.android.synthetic.main.fragment_sign_up_birthdate.*

class SignUpBirthdateFragment : Fragment() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
	}

	override fun onAttach(context: Context?) {
		super.onAttach(context)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_sign_up_birthdate, container, false)
	}

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)

		ArrayAdapter(
			activity,
			android.R.layout.simple_list_item_1,
			resources.getStringArray(R.array.short_months)
		).let {
			it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
			spinnerMonths.adapter = it
		}

		ArrayAdapter(
			activity,
			android.R.layout.simple_list_item_1,
			(1..31).toList()).let {
			it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
			spinnerDays.adapter = it
		}

		ArrayAdapter(
			activity,
			android.R.layout.simple_list_item_1,
			(2005.downTo(1900)).toList()).let {
			it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
			spinnerYears.adapter = it
		}
	}
}
