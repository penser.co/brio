package io.birr.brio_main.ui.activities

import android.Manifest
import android.Manifest.permission.READ_SMS
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import io.birr.brio_main.R
import io.birr.brio_main.model.getPrefString
import io.birr.brio_main.ui.adapters.TransactionListAdapter
import io.birr.brio_main.utils.ResponsePatterns.IMPLICIT_BALANCE
import io.birr.brio_main.utils.SmsHandler.getCbeSmsList
import io.birr.brio_main.utils.SmsHandler.isImplicitBalance
import io.birr.brio_main.utils.SmsHandler.isTxn
import io.birr.brio_main.utils.cbeUssdCall
import io.birr.brio_main.utils.checkPermissions
import io.birr.brio_main.utils.refreshLocale
import io.birr.brio_main.utils.withPermission
import kotlinx.android.synthetic.main.activity_transaction.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.textColor
import org.jetbrains.anko.uiThread
import org.ocpsoft.prettytime.PrettyTime
import java.util.*

class TransactionActivity : AppCompatActivity() {

	private val _rootRef = FirebaseDatabase.getInstance().reference
	private var _auth: FirebaseAuth? = null

	private var _savedPin: String? = ""
	private var _runnable: Runnable? = null
	private var _isRunning: Boolean = true
	private var _adapter: TransactionListAdapter? = null

	private var _cbeSmsList: List<List<Pair<String, String>>> = listOf()

	private var _autoBalanceCheck = true

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setContentView(R.layout.activity_transaction)

		_isRunning = true
		watchChanges()

		_savedPin = getPrefString("pin_code")

		swipeRefresh.setOnRefreshListener {
			checkAccountBalance()

			Handler().postDelayed({
				swipeRefresh.isRefreshing = false
			}, 1500)
		}

		checkPermissions(READ_SMS) {
			doAsync {
				_cbeSmsList = getCbeSmsList(this@TransactionActivity)

				uiThread {
					setTxnListData()
					updateBalance()
				}
			}
		}
	}

	override fun onResume() {
		super.onResume()
		_isRunning = true
		watchChanges()

		refreshLocale()
		updateData()
	}

	override fun onPause() {
		super.onPause()
		_isRunning = false
	}

	private fun watchChanges() {
		val handler = Handler()

		_runnable = object : Runnable {
			override fun run() {
				doAsync {

					withPermission(READ_SMS) {
						// TODO: getting all the sms is making the ui laggy... optimize me, please?
						// TODO: also, don't parse sms everytime, write to db and only parse new sms
						_cbeSmsList = getCbeSmsList(this@TransactionActivity)

						uiThread {
							updateBalance()
							_adapter = TransactionListAdapter(
								this@TransactionActivity,
								_cbeSmsList.filter {
									isTxn(it)
								}
							)

							_adapter?.notifyDataSetChanged()
						}
					}
				}

				if (_isRunning) {
					handler.postDelayed(this, 10000)
				}
			}
		}

		handler.postDelayed(_runnable, 0)
	}

	private fun updateData() {
		withPermission(Manifest.permission.READ_SMS) {
			setTxnListData()
			updateBalance()
		}
	}

	private fun updateBalance() {
		doAsync {
			val balanceResponses = _cbeSmsList.filter {
				val body = it.find {
					it.first == "body"
				}?.second ?: ""

				isImplicitBalance(body)
			}

			val latestBalanceObj = balanceResponses.maxBy {
				val date = it.find { it.first == "date" }?.second ?: "0"

				date.toLong()
			}

			val latestBalanceBody = latestBalanceObj?.find { it.first == "body" }?.second
			val latestBalanceTime = latestBalanceObj?.find { it.first == "date" }.let {
				if (it != null) it.second else "0"
			}

			val latestBalanceDate = Date((latestBalanceTime).toLong())
			val prettyTimeBalance = PrettyTime().format(latestBalanceDate)

			val latestBalance = latestBalanceBody?.replace(
				IMPLICIT_BALANCE.toRegex(), { it.groupValues[1] }
			) ?: "0.00"

			val balanceRegex = """^[0-9]+(?:\.[0-9]+)?$""".toRegex()

			val currentBalanceText = if (latestBalance.matches(balanceRegex)) {
				latestBalance
			} else {
				"0.00"
			}

			val currentBalanceColor = ContextCompat.getColor(
				this@TransactionActivity, R.color.primary50
			)

			val balanceLastUpdatedText = if (latestBalanceTime == "0") {
				getString(R.string.balance_never_checked)
			} else {
				String.format(
					getString(R.string.last_updated_date),
					prettyTimeBalance
				)
			}

			uiThread {
				currentBalance.text = currentBalanceText
				currentBalance.textColor = currentBalanceColor
				balanceLastUpdated.text = balanceLastUpdatedText

				_savedPin?.let { pin ->
					if (_autoBalanceCheck) {
						_autoBalanceCheck = false

						if (Date().time - latestBalanceDate.time > 1000 * 60 * 60 * 6) {
							cbeUssdCall(6, pin)
						}
					}
				}
			}
		}
	}

	private fun setTxnListData() {
		_adapter = TransactionListAdapter(
			this@TransactionActivity,
			_cbeSmsList.filter {
				isTxn(it)
			}
		)

		transactionList.adapter = _adapter
	}

	fun doCheckAccountBalance(view: View) {
		checkAccountBalance()
	}

	private fun checkAccountBalance() {
		_savedPin?.let { pin ->
			cbeUssdCall(6, pin)
		}
	}
}