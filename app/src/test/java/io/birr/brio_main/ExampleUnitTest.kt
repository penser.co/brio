package io.birr.brio_main

import io.birr.brio_main.utils.date.*
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
	@Test
	fun addition_isCorrect() {
		assertEquals(4, 2 + 2)
	}

	@Test
	fun ethiopian_date_calculation () {
		assertEquals(mapOf(
                Pair("day", 2),
                Pair("month", 6),
                Pair("year", 1991)
        ) , convertFromJulian(9,2,1999)) //my bd :)
	}
}
